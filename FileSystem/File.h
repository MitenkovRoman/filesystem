#pragma once

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <time.h>
using namespace std;

void checkName(const string & n);
string makeName(const string & n);

/**��������� ��� �������� ���� ������� � ����� ��� ������������ c ��������������� ID_owner*/
struct AccessFileRights{
	int ID_owner;
	map<const string, bool> rights;
	AccessFileRights(int id, bool r);
};

/**��������� ��� �������� ���� ������� � �������� ��� ������������ c ��������������� ID_owner*/
struct AccessCatalogRights{
	int ID_owner;
	map<const string, bool> rights;
	AccessCatalogRights(int id, bool r);
};

/**A���������� ������� ����� ��� ������� ������ � ���������*/
class File{
protected:
	int ID_user;///<������������� ������������-��������� ����� ��� ��������
	int ID_group;///<������������� ������-��������� ����� ��� ��������
	int adress; ///<����������� ������ ���������� ����� �� �����
	string placeInFS;
	struct tm makingTime;///<����� �������� ����� ��� ��������
public:
	virtual ~File(){}
	virtual File * clone()const = 0; ///<������� �������� ����� ������ \return ����� ���������� ������
	virtual void add_user(int id_user, bool rights) = 0; ///<������� ���������� ������������ � ��� ���� ������� � ��������� �������� ���� ������� \param id_user ������������� ������������ param rights ������� ���� �������
	virtual void delete_user(int id_user) = 0;///<������� �������� ������������ �� ��������� �������� ���� ������� \param id_user ������������� ������������
	virtual void add_group(int id_group, bool rights) = 0;///<������� ���������� ������ � � ���� ������� � ��������� �������� ���� ������� \param id_group ������������� ������ param rights ������� ���� ������� 
	virtual void delete_group(int id_group) = 0;///<������� �������� ������ �� ��������� �������� ���� ������� \param id_group ������������� ������
	struct tm get_makingTime()const{ return makingTime; }///<������� ��������� ���� � ������� �������� �����
	void set_makingTime(struct tm new_time){ makingTime = new_time; }///<������� ��������� ���� � ������� �������� �����
	string get_place()const{ return placeInFS; }
	void set_place(const string & new_place){ placeInFS = new_place; }
	virtual void change_user_right(int id_user, const string & right) = 0;///<������� ��������� ����� ������� right ��� ������������ � ��������������� id_user
	virtual void change_group_right(int id_group, const string & right) = 0;///<������� ��������� ����� ������� right ��� ������ � ��������������� id_user
	virtual bool check_right(int id_user, const string & right, vector<int>)const = 0;///<������� �������� ����� ������� right ��� ������������ � ��������������� id_user, ��������� � ������ � ���������������� vector<int>
	int get_ID_user()const{ return ID_user; }///<������� ��������� �������������� ������������-��������� ����� ��� ��������
	int get_ID_group()const{ return ID_group; }///<������� ��������� �������������� ������-��������� ����� ��� ��������
	void set_ID_user(int id){ ID_user = id; }///<������� ��������� �������������� ������������-��������� ����� ��� ��������
	void set_ID_group(int id){ ID_group = id; }///<������� ��������� �������������� ������-��������� ����� ��� ��������
};

class UsualFile : public File{
private:
	unsigned int size;///<������ ����� � ������
	string data;///<���������� �����
	vector<AccessFileRights> group_rights;///<����� ������� � ������� ����� ��� ���� ����� �������� �������
	vector<AccessFileRights> user_rights;///<����� ������� � ������� ����� ��� ���� ������������� �������� �������
	bool lock_read;///<���������� ������
	bool lock_write;///<���������� ������
public:
	UsualFile(int id_user, int id_group, vector<AccessFileRights> & u_rights, vector<AccessFileRights> & g_rights);
	~UsualFile(){}
	string get_data()const{ return data; }
	vector<AccessFileRights> get_users_rights()const{ return user_rights; }
	vector<AccessFileRights> get_groups_rights()const{ return group_rights; }
	void add_user(int, bool);///<������������� ����� ����������� ������� �� ������ File
	void delete_user(int id_user);///<������������� ����� ����������� ������� �� ������ File
	void add_group(int, bool);///<������������� ����� ����������� ������� �� ������ File
	void delete_group(int id_group);///<������������� ����� ����������� ������� �� ������ File
	AccessFileRights get_user_rights(int id_user);///<������� ��������� ���� ������� ��� ������������ � ��������������� id_user
	UsualFile * clone()const{ return new UsualFile(*this); }///<������������� ����� ����������� ������� �� ������ File
	void change_user_right(int, const string &);///<������������� ����� ����������� ������� �� ������ File
	void change_group_right(int id_group, const string & right);///<������������� ����� ����������� ������� �� ������ File
	bool check_right(int id_user, const string & right, vector<int>)const;///<������������� ����� ����������� ������� �� ������ File
	void Write(const string & str, int position);///<������� ������ � ���� ������ str � ������� position
	string Read(int position, int number);///<������� ������ �� ����� number �������� � ������� position
};

class Catalog : public File{
private:
	map<const string, File*> catalog;///<��������� �������� (����� ������ � ������������, ����������� � �������� � ��������� �� ���)
	vector<AccessCatalogRights> group_rights;///<����� ������� � ������� �������� ��� ���� ����� �������� �������
	vector<AccessCatalogRights> user_rights;///<����� ������� � ������� �������� ��� ���� ������������� �������� �������
public:
	Catalog(int, int, vector<AccessCatalogRights> &, vector<AccessCatalogRights> &);
	virtual ~Catalog(){ clear(); }
	const map<const string, File*> & get_catalog()const{ return catalog; }///<������� ��������� ��������� ������� ��������
	int get_size()const{ return catalog.size(); }///<������� ��������� ���������� ������ � ������������ � ��������� ��������
	const string & get_file_name(int i)const;///<������� ��������� ����� i-��� ����� ��� ����������� � ��������� ��������
	vector<AccessCatalogRights> get_users_rights()const{ return user_rights; }
	vector<AccessCatalogRights> get_groups_rights()const{ return group_rights; }
	AccessCatalogRights get_user_rights(int id);///<������� ��������� ���� ������� ��� ������������ � ��������������� id
	Catalog * clone()const;///<������������� ����� ����������� ������� �� ������ File
	void change_user_right(int, const string &);///<������������� ����� ����������� ������� �� ������ File
	void change_group_right(int id_group, const string & right);///<������������� ����� ����������� ������� �� ������ File
	bool check_right(int id_user, const string & right, vector<int>)const;///<������������� ����� ����������� ������� �� ������ File
	void clear();///<������� ������� �������� (�������� ���� ������ � ������������ �������� � ����)
	void add_user(int, bool);///<������������� ����� ����������� ������� �� ������ File
	void delete_user(int id_user);///<������������� ����� ����������� ������� �� ������ File
	void add_group(int, bool);///<������������� ����� ����������� ������� �� ������ File
	void delete_group(int id_group);///<������������� ����� ����������� ������� �� ������ File
	void add_to_catalog(string name, File * file);///<������� ���������� ����� ��� �������� file � ������ name � ������ �������
	void delete_from_catalog(string name);///<������� �������� ����� ��� �������� � ������ name �� ������� ��������
	void Rename_catalog(const string & old_name, const string & new_name);///<������� �������������� ����� ��� ����������� ��������� � ������ ������� � ������ old_name
	File * findItem(const string & name);///<������� ���������� ����� ��� ����������� � ������ name
	friend File * clone_catalog(const File * ptr, File * result);///<������� - ���� ������, ���������� ������� ��� ���� ptr � result
	friend void delete_catalog(File * ptr);///<������� - ���� ������, ��������� ���������� �������� ptr
	friend void clear_catalog(File * ptr);///<������� - ���� ������, ��������� ���������� �������� ptr
};