#pragma once
#include <iostream>
#include <vector>
#include <string>

using namespace std;

/**����������� ������� ����� ���������� ������ � ���������*/
class Owner{
protected:
	int ID;///<������������� ���������
	string name;///<��� ���������
public:
	void setID(int id){ ID = id; }
	virtual int makeNewID() = 0;///<������� ������������ ����� �������������
	virtual Owner * clone() = 0;///<������� ��������� ����� ���������� ������
	virtual ~Owner(){}
};

class User : public Owner{
	vector<int> groups;///<������ ��������������� �����, � ������� ������ ������������ ������
public:
	User(string u_name){ Owner::ID = makeNewID(); Owner::name = u_name; }
	virtual ~User(){}
	int makeNewID(){ static int id = -1; id++; return id; }///<������������� ����� ����������� ������� ������ Owner
	int get_ID()const{ return Owner::ID; }///<������� ��������� �������������� ������������
	vector<int> get_groups()const{ return groups; }///<������� ��������� ������� ��������������� �����, � ������� ������ ������������ ������
	User * clone(){ return new User(*this); }///<������������� ����� ����������� ������� ������ Owner
	string get_name()const{ return Owner::name; }///<������� ��������� ����� ������������
	void Add(int id_group){ groups.push_back(id_group); }///<������� ���������� �������������� ������ � ������ ��������������� �����, � ������� ������ ������������ ������
	void Delete(int id);///<������� �������� �������������� ������ �� ������� ��������������� �����, � ������� ������ ������������ ������
};

class Group : public Owner{
	vector<int> users;///<������ ��������������� �������������, ������� ������ � ��� ������
public:
	Group(string g_name){ Owner::ID = makeNewID(); Owner::name = g_name; }
	virtual ~Group(){}
	int makeNewID(){ static int id = -1; id++; return id; }///<������������� ����� ����������� ������� ������ Owner
	string get_name()const{ return Owner::name; }///<������� ��������� ����� ������
	int get_ID()const{ return Owner::ID; }///<������� ��������� �������������� ������
	vector<int> get_users()const{ return users; }///<������� ��������� ������� ��������������� �������������, ������� ������ � ������ ������
	Group * clone(){ return new Group(*this); }///<������������� ����� ����������� ������� ������ Owner
	void Add(int id_user){ users.push_back(id_user); }///<������� ���������� �������������� ������ � ������ ��������������� �������������, ������� � ������ ������ ������
	void Delete(int id);///<������� �������� �������������� ������ �� ������� ��������������� �������������, ������� � ������ ������ ������
};

template<class T>
class VectorIt{
private:
	T * ptr;
public:
	VectorIt() : ptr(nullptr){}
	VectorIt(T * object) : ptr(object){}
	~VectorIt(){}
	bool operator == (const VectorIt & p)const{ return ptr == p.ptr; }
	bool operator != (const VectorIt & p)const{ return ptr != p.ptr; }
	VectorIt & operator ++() { ++ptr;  return *this; }
	VectorIt operator ++ (int) { ++ptr; return *this; }
	T & operator *(){ return *ptr; }
};

template<class T>
class Vector{
private:
	T * arr;
	int arr_size;
	int countBlocks;
	int sizeBlock;
public:
	Vector() : arr(nullptr), arr_size(0), countBlocks(1), sizeBlock(10){
		try{
			arr = new T[countBlocks * sizeBlock * sizeof(T)];
		}
		catch (bad_alloc &){
			throw exception("Memory allocation error!!!");
		}
	}

	Vector(const Vector & item){
		try{
			arr = new T[item.countBlocks * item.sizeBlock * sizeof(T)];
			for (int i = 0; i < item.size(); i++)
				arr[i] = item.arr[i];
			arr_size = item.arr_size;
			countBlocks = item.countBlocks;
			sizeBlock = item.sizeBlock;
		}
		catch (bad_alloc &){
			throw exception("Memory allocation error!!!");
		}
	}

	Vector(Vector && item){
		arr = item.arr;
		item.arr = nullptr;
		arr_size = item.arr_size;
		countBlocks = item.countBlocks;
		sizeBlock = item.sizeBlock;
	}

	Vector & operator = (const Vector & item){
		if (&item != this){
			try{
				delete[] arr;
				arr = new T[item.countBlocks * item.sizeBlock * sizeof(T)];
				for (int i = 0; i < item.size(); i++)
					arr[i] = item[i];
				arr_size = item.arr_size;
				countBlocks = item.countBlocks;
				sizeBlock = item.sizeBlock;
			}
			catch (bad_alloc &){
				throw exception("Memory allocation error!!!");
			}
		}
		return *this;
	}

	Vector & operator = (Vector && item){
		if (&item != this){
			delete[] arr;
			arr = item.arr;
			item.arr = nullptr;
			arr_size = item.arr_size;
			countBlocks = item.countBlocks;
			sizeBlock = item.sizeBlock;
		}
		return *this;
	}

	~Vector(){ delete[] arr; }
	template<class T>
	friend class VectorIt;
	typedef VectorIt<T> Iterator;
	Iterator begin()const { if (arr_size) return &(arr[0]); else return nullptr; }
	Iterator end()const { if (arr_size) return &arr[arr_size - 1] + 1; else return nullptr; }
	int size()const{ return arr_size; }
	Vector<T> * clone(){ return new Vector<T>(*this); }
	void push_back(const T & item){
		if ((countBlocks * sizeBlock * sizeof(T) - sizeof(T) * arr_size) >= 0){
			arr[arr_size] = item;
			arr_size++;
		}
		else{
			countBlocks++;
			T * arr2 = new T[countBlocks * sizeBlock * sizeof(T)];
			for (int i = 0; i < arr_size; i++)
				arr2[i] = arr[i];
			arr2[arr_size] = item;
			delete[] arr;
			arr = arr2;
			arr_size++;
		}
	}
	void erase(int first, int number = 1){
		for (int i = first + number; i < arr_size; i++)
			arr[i - number] = arr[i];
		arr_size -= number;
		if (countBlocks * sizeBlock - arr_size > sizeBlock){
			countBlocks = arr_size / sizeBlock + 1;
			T * arr2 = new T[countBlocks * sizeBlock * sizeof(T)];
			for (int i = 0; i < arr_size; i++)
				arr2[i] = arr[i];
			delete[] arr;
			arr = arr2;
		}
	}
	void erase(Iterator ptr){
		Vector<T>::Iterator pred = nullptr, p;
		for (int i = 0; i < arr_size; i++)
			if (*(ptr) == arr[i])
				pred = &(arr[i]);
		if (pred == nullptr)
			throw exception("Element was not found!!!");
		for (p = pred, ++p; p != end(); ++pred, ++p)
			*(pred) = *(p);
		arr_size--;
		if (countBlocks * sizeBlock - arr_size > sizeBlock){
			countBlocks = arr_size / sizeBlock + 1;
			T * arr2 = new T[countBlocks * sizeBlock * sizeof(T)];
			for (int i = 0; i < arr_size; i++)
				arr2[i] = arr[i];
			delete[] arr;
			arr = arr2;
		}
	}
	T operator [](int i)const{ if (i < 0 || i > arr_size) throw exception("Incorrect index!!!"); return arr[i]; }
	T & operator [](int i){ if (i < 0 || i > arr_size) throw exception("Incorrect index!!!"); return arr[i]; }
};

class TableUser{
private:
	Vector<User *> arr;///<������ �������������, �������� � �������
public:
	TableUser(){}
	TableUser(const TableUser &);
	virtual ~TableUser();
	Vector<User *> get_arr()const { return arr; }
	int number()const{ return arr.size(); }///<������� ��������� ���������� ������������� � �������
	int get_ID(int i)const{ return arr[i]->get_ID(); }///<������� ��������� �������������� i-��� ������������ � �������
	void Add(User * u){ arr.push_back(u->clone()); }///<������� ���������� ������������ � �������
	User * Find(const string & uname)const;///<������� ������ ������������ � ������� �� �����
	User * Find(int ID)const;///<������� ������ ������������ � ������� �� ��������������
	void Delete(const string & name);///<������� �������� ������������ � ������ name �� ������� 
};

class TableGroup{
private:
	vector<Group *> arr;
public:
	TableGroup(){}
	TableGroup(const TableGroup &);
	virtual ~TableGroup();
	vector<Group *> get_arr()const{ return arr; }
	int number()const{ return arr.size(); }///<������� ��������� ���������� ����� � �������
	int get_ID(int i)const{ return arr[i]->get_ID(); }///<������� ��������� �������������� i-�� ������ � �������
	void Add(Group * g){ arr.push_back(g->clone()); }///<������� ���������� ������ � �������
	Group * Find(const string & gname)const;///<������� ������ ������ � ������� �� �����
	Group * Find(int ID)const;///<������� ������ ������ � ������� �� ��������������
	void Delete(const string & str);///<������� �������� ������ � ������ name �� ������� 
};
