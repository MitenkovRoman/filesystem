#include "stdafx.h"
#include "FileSystem.h"
#include <typeinfo>

bool file_in_catalog(const string & catal, const string & adress);

FileSystem::FileSystem(){
	try{
		Group * admins = new Group("Administrators");
		User * admin = new User("Administrator");
		admins->Add(admin->get_ID());
		table_user.Add(admin);
		table_group.Add(admins);
		ID_current_user = -1;

		vector<AccessCatalogRights> catalog_rights;
		AccessCatalogRights rights(admin->get_ID(), true);
		catalog_rights.push_back(rights);
		root = new Catalog(admin->get_ID(), admins->get_ID(), catalog_rights, catalog_rights);
		root->set_place("");
		root_name = "root";

		SimpleCommand * copy = new SimpleCommand(&FileSystem::Copy, 2);
		table_command.Add("copy", copy);
		SimpleCommand * move = new SimpleCommand(&FileSystem::Move, 2);
		table_command.Add("move", move);
		SimpleCommand * createCatalog = new SimpleCommand(&FileSystem::CreateCatalog, 3);
		table_command.Add("create catalog", createCatalog);
		SimpleCommand * createFile = new SimpleCommand(&FileSystem::CreateFile, 3);
		table_command.Add("create file", createFile);
		SimpleCommand * renameItem = new SimpleCommand(&FileSystem::Rename, 2);
		table_command.Add("rename", renameItem);
		SimpleCommand * showFileSystem = new SimpleCommand(&FileSystem::Show, 0);
		table_command.Add("show", showFileSystem);
		SimpleCommand * deleteItem = new SimpleCommand(&FileSystem::Delete, 1);
		table_command.Add("delete", deleteItem);
		SimpleCommand * writeFile = new SimpleCommand(&FileSystem::Write, 3);
		table_command.Add("write", writeFile);
		SimpleCommand * readFile = new SimpleCommand(&FileSystem::Read, 3);
		table_command.Add("read", readFile);
		SimpleCommand * userchangeRight = new SimpleCommand(&FileSystem::UserRightChange, 3);
		table_command.Add("change user right", userchangeRight);
		SimpleCommand * groupchangeRight = new SimpleCommand(&FileSystem::GroupRightChange, 3);
		table_command.Add("change group right", groupchangeRight);
		SimpleCommand * addUserInGroup = new SimpleCommand(&FileSystem::Add_user_in_group, 2);
		table_command.Add("add user in group", addUserInGroup);
		SimpleCommand * deleteUserFromGroup = new SimpleCommand(&FileSystem::Delete_user_from_group, 2);
		table_command.Add("delete user from group", deleteUserFromGroup);
		SimpleCommand * deleteGroup = new SimpleCommand(&FileSystem::Delete_group, 1);
		table_command.Add("delete group", deleteGroup);
		SimpleCommand * deleteUser = new SimpleCommand(&FileSystem::Delete_user, 1);
		table_command.Add("delete user", deleteUser);
		SimpleCommand * createCommand = new SimpleCommand(&FileSystem::CreateCommand, -1);
		table_command.Add("create command", createCommand);
		SimpleCommand * deleteCommand = new SimpleCommand(&FileSystem::DeleteCommand, 1);
		table_command.Add("delete command", deleteCommand);
	}
	catch (bad_alloc){
		throw exception("Memory allocation error!!!");
	}
}

TableCommand::~TableCommand(){
	map<const string, Command*>::iterator ptr;
	for (ptr = arr.begin(); ptr != arr.end(); ++ptr){
		delete ptr->second;
		ptr->second = nullptr;
	}
}

string FileSystem::CreateCatalog(vector<const string> str){//��� ������������ �������� � ����� ��������, � ������� ����� ������� �������, ��� ��������� ������
	File * ptr;
	vector<const string> rights;
	string name_group;
	rights.push_back("change");
	if (str.size() < 3)
		throw exception("Wrong number of operands!!!");
	if (str[2].empty())
		name_group = "Administrators";
	else{
		if (!table_group.Find(str[2]))
			throw exception("Group with this name was not found!!!");
		name_group = str[2];
	}
	checkName(str[0]);
	if (!(ptr = Find(str[1], rights)))
		throw exception("Catalog was not found!!!");
	if (typeid(*ptr) == typeid(Catalog)){
		Catalog * p = static_cast<Catalog *>(ptr);
		vector<AccessCatalogRights> catalog_user_rights, catalog_group_rights;
		AccessCatalogRights r1(0, true), r2(0, false);
		for (int i = 0; i < table_user.number(); i++)
			if (table_user.get_ID(i) == 0 || table_user.get_ID(i) == ID_current_user){
				r1.ID_owner = table_user.get_ID(i);
				catalog_user_rights.push_back(r1);
			}
			else{
				r2.ID_owner = table_user.get_ID(i);
				catalog_user_rights.push_back(r2);
			}
			for (int i = 0; i < table_group.number(); i++)
				if (table_group.get_ID(i) == 0 || table_group.get_ID(i) == table_group.Find(name_group)->get_ID()){
					r1.ID_owner = table_group.Find(name_group)->get_ID();
					catalog_group_rights.push_back(r1);
				}
				else{
					r2.ID_owner = table_group.get_ID(i);
					catalog_group_rights.push_back(r2);
				}
				Catalog ctl(ID_current_user, table_group.Find(name_group)->get_ID(), catalog_user_rights, catalog_group_rights);
				ctl.set_place(str[1]);
				p->add_to_catalog(str[0], &ctl);
	}
	else
		throw exception("Catalog can be made only in catalog!!!");
	return string();
}

string FileSystem::CreateFile(vector<const string> str){// ��� ������������ ����� � ����� ��������, � ������� ����� ������� ���� � ��� ��������� ������
	File * ptr;
	vector<const string> rights;
	string name_group;
	rights.push_back("change");
	if (str.size() < 3)
		throw exception("Wrong number of operands!!!");
	if (str[2].empty())
		name_group = "Administrators";
	else{
		if (!table_group.Find(str[2]))
			throw exception("Group with this name was not found!!!");
		name_group = str[2];
	}
	checkName(str[0]);
	if (!(ptr = Find(str[1], rights)))
		throw exception("File or catalog was not found!!!");
	if (typeid(*ptr) == typeid(Catalog)){
		Catalog * p = static_cast<Catalog *>(ptr);
		vector<AccessFileRights> file_user_rights, file_group_rights;
		AccessFileRights r1(0, true), r2(0, false);
		for (int i = 0; i < table_user.number(); i++)
			if (table_user.get_ID(i) == 0 || table_user.get_ID(i) == ID_current_user){
				r1.ID_owner = table_user.get_ID(i);
				file_user_rights.push_back(r1);
			}
			else{
				r2.ID_owner = table_user.get_ID(i);
				file_user_rights.push_back(r2);
			}
			for (int i = 0; i < table_group.number(); i++)
				if (table_group.get_ID(i) == 0 || table_group.get_ID(i) == table_group.Find(name_group)->get_ID()){
					r1.ID_owner = table_group.Find(name_group)->get_ID();
					file_group_rights.push_back(r1);
				}
				else{
					r2.ID_owner = table_group.get_ID(i);
					file_group_rights.push_back(r2);
				}
				UsualFile file(ID_current_user, table_group.Find(name_group)->get_ID(), file_user_rights, file_group_rights);
				file.set_place(str[1]);
				p->add_to_catalog(str[0], &file);
	}
	else
		throw exception("File can be made only in catalog!!!");
	return string();
}

void FileSystem::changePlaces(File * ptr, const string & old_adress, const string & new_adress){
	if (typeid(*ptr) == typeid(Catalog)){
		Catalog * ctl = static_cast<Catalog*>(ptr);
		map<const string, File*> v = ctl->get_catalog();
		map<const string, File*>::iterator ptr;
		for (ptr = v.begin(); ptr != v.end(); ++ptr)
			changePlaces(ptr->second, old_adress, new_adress);
	}
	string s = ptr->get_place(), res;
	s.erase(0, old_adress.size());
	res = new_adress;
	if (old_adress == "")
		res += '\\';
	res += s;
	ptr->set_place(res);
}

string FileSystem::Copy(vector<const string> str){ //����� ����������� �������� � ����� �����, ���� ���������� �������
	File * file, *ptr;
	vector<const string> rights;
	vector<const string> null_rights;
	rights.push_back("change");
	if (str.size() < 2)
		throw exception("Wrong number of operands!!!");
	if (!(file = Find(str[0], null_rights)) || !(ptr = Find(str[1], rights)))
		throw exception("File or catalog was not found!!!");
	if (typeid(*ptr) == typeid(Catalog)){
		File * ctlg = Find_place(str[0], null_rights);
		Catalog * from = static_cast<Catalog *>(ctlg);
		Catalog * to = static_cast<Catalog *>(ptr);
		File * f = file->clone();
		time_t time_now = time(NULL);
		struct tm makingTime;
		localtime_s(&makingTime, &time_now);
		f->set_makingTime(makingTime);
		if (typeid(*f) == typeid(Catalog))
				changePlaces(f, f->get_place(), str[1]);
		f->set_place(str[1]);
		to->add_to_catalog(makeName(str[0]), f);
	}
	else
		throw exception("To copy files in file is impossible!!!");
	return string();
}

string FileSystem::Move(vector<const string> str){ //����� ������������� �������� � ����� �����, ���� ������������ �������
	File * file, *ptr;
	vector<const string> rights;
	vector<const string> null_rights;
	rights.push_back("change");
	if (str.size() < 2)
		throw exception("Wrong number of operands!!!");
	if (!(file = Find(str[0], rights)) || !(ptr = Find(str[1], rights)))
		throw exception("File or catalog was not found!!!");
	if (file_in_catalog(str[0], str[1]))
		throw exception("Catalog can't be added in catalog that is contained in it!!!");
	if (typeid(*ptr) == typeid(Catalog)){
		File * ctlg = Find_place(str[0], rights);
		Catalog * from = static_cast<Catalog *>(ctlg);
		Catalog * to = static_cast<Catalog *>(ptr);
		File * f = file->clone();
		if (typeid(*f) == typeid(Catalog))
			changePlaces(f, f->get_place(), str[1]);
		f->set_place(str[1]);
		to->add_to_catalog(makeName(str[0]), f);
		from->delete_from_catalog(makeName(str[0]));
	}
	else
		throw exception("To move files in file is impossible!!!");
	return string();
}

string FileSystem::Delete(vector<const string> str){ //����� ���������� ��������
	File * file, *ctlg;
	vector<const string> rights;
	rights.push_back("change");
	if (str.size() < 1)
		throw exception("Wrong number of operands!!!");
	if (!(file = Find(str[0], rights)))
		throw exception("File or catalog was not found!!!");
	if (!(ctlg = Find_place(str[0], rights))){
		delete root;
		root = nullptr;
		return string();
	}
	Catalog * ctl = static_cast<Catalog *>(ctlg);
	if (typeid(*file) == typeid(Catalog)){
		Catalog * c = static_cast<Catalog *>(file);
		c->clear();
	}
	ctl->delete_from_catalog(makeName(str[0]));
	return string();
}

string FileSystem::Rename(vector<const string> str){ //����� ��������, ������� ����� ������������� � ����� ���
	vector<const string> rights;
	rights.push_back("change meta data");
	if (str.size() < 2)
		throw exception("Wrong number of operands!!!");
	checkName(str[1]);
	if (!Find(str[0], rights))
		throw exception("File was not found!!!");
	File * ctlg;
	if (!(ctlg = Find_place(str[0], rights))){
		set_root_name(str[1]);
		return string();
	}
	Catalog * ctl = static_cast<Catalog *>(ctlg);
	ctl->Rename_catalog(makeName(str[0]), str[1]);
	return string();
}

void FileSystem::print(ostream & c, File * ptr, int level)const{
	int i, j;
	if (typeid(*ptr) == typeid(Catalog)){
		Catalog * ctl = static_cast<Catalog*>(ptr);
		map<const string, File*> v = ctl->get_catalog();
		map<const string, File*>::iterator ptr;
		for (ptr = v.begin(); ptr != v.end(); ++ptr){
			for (j = 0; j < 4 * level; j++)
				c << " ";
			if (typeid(*(ptr->second)) == typeid(Catalog)){
				Catalog * ctl = static_cast<Catalog *>(ptr->second);
				c << "Catalog:\"" << ptr->first << "\" making time: " << ptr->second->get_makingTime().tm_hour << ":" <<
					ptr->second->get_makingTime().tm_min << ":" << ptr->second->get_makingTime().tm_sec << endl;
				if (!ctl->check_right(ID_current_user, "pass", table_user.Find(ID_current_user)->get_groups()))
					continue;
			}
			else
				c << "File:\"" << ptr->first << "\" making time: " << ptr->second->get_makingTime().tm_hour << ":" <<
				ptr->second->get_makingTime().tm_min << ":" << ptr->second->get_makingTime().tm_sec << endl;
			print(c, ptr->second, level + 1);
		}
	}
}

ostream & operator << (ostream & c, const FileSystem & FS){
	c << "File System:" << endl;
	if (FS.get_root()){
		c << "  Catalog:\"" << FS.get_root_name() << "\"" << endl;
		FS.print(c, FS.root, 1);
	}
	return c;
}

string first_name(const string & str, int & index){
	string s;
	int i;
	for (i = index; i < str.size() && str[i] != '\\'; i++)
		s += str[i];
	index = i + 1;
	return s;
}

File * FileSystem::Find(const string & str, vector<const string> rights){//����� ����� ��� �������� � ����� ���� ������� ����� ��������� �� ����
	File * file;
	string s;
	int i = 0;
	Catalog * ctl = root;
	UsualFile * u_file = nullptr;
	file = root;
	s = first_name(str, i);
	if (s != get_root_name())
		throw exception("Wrong adress!!!");

	for (int j = 0; j < rights.size(); j++)
		if (!ctl->check_right(ID_current_user, rights[j], table_user.Find(ID_current_user)->get_groups()))
			throw exception("You don't have a right to do this action!!!");
	if (!ctl->check_right(ID_current_user, "pass", table_user.Find(ID_current_user)->get_groups()))
		throw exception("You don't have a right to do this action!!!");

	while (i < str.size()){
		s = first_name(str, i);
		if (file = ctl->findItem(s)){
			if (typeid(*file) == typeid(Catalog)){
				ctl = static_cast<Catalog*>(file);
				for (int j = 0; j < rights.size(); j++)
					if (!ctl->check_right(ID_current_user, rights[j], table_user.Find(ID_current_user)->get_groups()))
						throw exception("You don't have a right to do this action!!!");
				if (!ctl->check_right(ID_current_user, "pass", table_user.Find(ID_current_user)->get_groups()))
					throw exception("You don't have a right to do this action!!!");
			}
			else{
				if (i < str.size())
					throw exception("Wrong adress!!!");
				u_file = static_cast<UsualFile *>(file);

				for (int j = 0; j < rights.size(); j++)
					if (!u_file->check_right(ID_current_user, rights[j], table_user.Find(ID_current_user)->get_groups()))
						throw exception("You don't have a right to do this action!!!");
			}

		}
		else
			return nullptr;
	}
	return file;
}

Catalog * FileSystem::Find_place(const string & str, vector<const string> rights){
	File * file;
	string s = str;
	int i;
	for (i = s.size() - 1; i >= 0 && s[i] != '\\'; i--)
		s.pop_back();
	if (i >= 0)
		s.pop_back();
	else
		return nullptr;
	if (!(file = Find(s, rights)))
		throw exception("File or catalog was not found!!!");
	if (file)
		return static_cast<Catalog *>(file);
	return nullptr;
}

Command * TableCommand::get_command(const string & str){
	if (arr.find(str) == arr.end())
		return nullptr;
	return arr.find(str)->second;
}

string FileSystem::Run(const string & str, vector<const string> operand){
	Command * com;
	if (ID_current_user < 0)
		throw exception("To use file system you should log on!!!");
	if (!(com = table_command.get_command(str)))
		throw exception("Command is wrong!!!");
	if (typeid(*com) == typeid(CompositeCommand)){
		CompositeCommand * c_com = static_cast<CompositeCommand*>(com);
		return c_com->Run(*this, c_com->Make_operand(operand));
	}
	return com->Run(*this, operand);
}

string FileSystem::Write(vector<const string> str){//���(�����) �����, ������ ��� ������, ������� ��� ������
	File * file;
	vector<const string> rights;
	rights.push_back("write");
	if (str.size() < 3)
		throw exception("Wrong number of operands!!!");
	if (!(file = Find(str[0], rights)))
		throw exception("File was not found!!!");
	if (typeid(*file) == typeid(UsualFile)){
		UsualFile * u_file = static_cast<UsualFile *>(file);
		u_file->Write(str[1], stoi(str[2]));
	}
	else
		throw exception("To write in catalog is impossible!!!");
	return string();
}
string FileSystem::Read(vector<const string> str){//���(�����) �����, ������� ��� ������, ���������� ��������
	File * file;
	vector<const string> rights;
	rights.push_back("read");
	if (str.size() < 3)
		throw exception("Wrong number of operands!!!");
	if (!(file = Find(str[0], rights)))
		throw exception("File was not found!!!");
	if (typeid(*file) == typeid(UsualFile)){
		UsualFile * u_file = static_cast<UsualFile *>(file);
		return u_file->Read(stoi(str[1]), stoi(str[2]));
	}
	else
		throw exception("To read catalog is impossible!!!");
}

bool file_in_catalog(const string & catal, const string & adress){
	int i;
	if (catal.size() > adress.size())
		return false;
	for (i = 0; i < catal.size(); i++)
		if (catal[i] != adress[i])
			return false;
	if (adress[i] == '\\')
		return true;
	return false;
}

void FileSystem::Start_work_with_user(const string & uname){
	User * u;
	if (ID_current_user >= 0)
		throw exception("Two users can't use file system at the same time!!!");
	if (u = Find_user(uname))
		ID_current_user = u->get_ID();
	else
		throw exception("User with this name was not found!!!");
}

void FileSystem::add_user(File * ptr, User * user){
	if (typeid(*ptr) == typeid(Catalog)){
		Catalog * ctl = static_cast<Catalog*>(ptr);
		map<const string, File *> ctlg = ctl->get_catalog();
		map<const string, File*>::iterator p;
		if (ctlg.size())
			for (p = ctlg.begin(); p != ctlg.end(); ++p)
				add_user(p->second, user);
	}
	if (ptr != root)
		ptr->add_user(user->get_ID(), false);
	else
		ptr->add_user(user->get_ID(), true);
}

void FileSystem::add_group(File * ptr, Group * group){
	if (typeid(*ptr) == typeid(Catalog)){
		Catalog * ctl = static_cast<Catalog*>(ptr);
		map<const string, File *> ctlg = ctl->get_catalog();
		map<const string, File*>::iterator p;
		if (ctlg.size())
			for (p = ctlg.begin(); p != ctlg.end(); ++p)
				add_group(p->second, group);
	}
	if (ptr != root)
		ptr->add_group(group->get_ID(), false);
	else
		ptr->add_group(group->get_ID(), true);
}

void FileSystem::delete_user(File * ptr, User * user){
	if (typeid(*ptr) == typeid(Catalog)){
		Catalog * ctl = static_cast<Catalog*>(ptr);
		map<const string, File *> ctlg = ctl->get_catalog();
		map<const string, File*>::iterator p, p1;
		if (ctlg.size())
			for (p = ctlg.begin(); p != ctlg.end(); ++ptr)
				delete_user(p->second, user);
	}
	if (ptr->get_ID_user() == user->get_ID())
		ptr->set_ID_user(0);
	ptr->delete_user(user->get_ID());
}

void FileSystem::delete_group(File * ptr, Group * group){
	if (typeid(*ptr) == typeid(Catalog)){
		Catalog * ctl = static_cast<Catalog*>(ptr);
		map<const string, File *> ctlg = ctl->get_catalog();
		map<const string, File*>::iterator p, p1;
		if (ctlg.size())
			for (p = ctlg.begin(); p != ctlg.end(); ++ptr)
				delete_group(p->second, group);
	}
	if (ptr->get_ID_group() == group->get_ID())
		ptr->set_ID_group(0);
	ptr->delete_group(group->get_ID());
}

void FileSystem::Add_new_user(const string & uname){
	if (Find_user(uname))
		throw exception("User with this name already exists!!!");
	User user(uname);
	table_user.Add(user.clone());
	this->add_user(root, &user);
}

string FileSystem::Delete_user(vector<const string> str){
	User * user;
	if (str.size() < 1)
		throw exception("Wrong number of operands!!!");
	if (ID_current_user)
		throw exception("Only Administrator can delete users!!!");
	if (str[0] == "Administrator")
		throw exception("Administrator can't be deleted!!!");
	if (!(user = Find_user(str[0])))
		throw exception("User was not found!!!");
	table_user.Delete(str[0]);
	this->delete_user(root, user);
	return string();
}

void FileSystem::Add_new_group(const string & gname){
	if (Find_group(gname))
		throw exception("Group with this name already exists!!!");
	Group group(gname);
	table_group.Add(group.clone());
	this->add_group(root, &group);
}

string FileSystem::Delete_group(vector<const string> str){
	Group * group;
	if (str.size() < 1)
		throw exception("Wrong number of operands!!!");
	if (ID_current_user)
		throw exception("Only Administrator can delete groups!!!");
	if (str[0] == "Administrators")
		throw exception("Administrators can't be deleted!!!");
	if (!(group = Find_group(str[0])))
		throw exception("Group was not found!!!");
	table_group.Delete(str[0]);
	this->delete_group(root, group);
	return string();
}

string FileSystem::Add_user_in_group(vector<const string> str){
	User * user;
	Group * group;
	bool fl;
	if (str.size() < 2)
		throw exception("Wrong number of parametrs!!!");
	if (ID_current_user){
		for (int i = 0; i < table_user.Find(ID_current_user)->get_groups().size(); i++)
			if (!table_user.Find(ID_current_user)->get_groups()[i])
				fl = true;
		if (!fl)
			throw exception("Only Administrator or users from Administrators group can add users in groups!!!");
	}
	if (!(user = Find_user(str[0])))
		throw exception("User with this name was not found!!!");
	if (!(group = Find_group(str[1])))
		throw exception("Group with this name was not found!!!");
	user->Add(group->get_ID());
	group->Add(user->get_ID());
	return string();
}

string FileSystem::Delete_user_from_group(vector<const string> str){
	User * user;
	Group * group;
	bool fl;
	if (str.size() < 2)
		throw exception("Wrong number of parametrs!!!");
	if (ID_current_user){
		for (int i = 0; i < table_user.Find(ID_current_user)->get_groups().size(); i++)
			if (!table_user.Find(ID_current_user)->get_groups()[i])
				fl = true;
		if (!fl)
			throw exception("Only Administrator or users from Administrators group can add users in groups!!!");
	}
	if (!(user = Find_user(str[0])))
		throw exception("User with this name was not found!!!");
	if (!(group = Find_group(str[1])))
		throw exception("Group with this name was not found!!!");
	user->Delete(group->get_ID());
	group->Delete(user->get_ID());
	return string();
}

string FileSystem::UserRightChange(vector<const string> str){//��� ����� ��� �������� � ����� ������� ���� ��������
	File * file;
	vector<const string> nullrights;
	if (str.size() < 3)
		throw exception("Wrong number of operands!!!");
	if (!(file = Find(str[0], nullrights)))
		throw exception("File or catalog was not found!!!");
	if (!table_user.Find(str[1]))
		throw exception("There are not this user in file system!!!");
	if (typeid(*file) == typeid(UsualFile)){
		UsualFile * u_file = static_cast<UsualFile*>(file);
		if (!u_file->check_right(ID_current_user, "change rights", table_user.Find(ID_current_user)->get_groups()))
			throw exception("You don't have a right to change rights of this file!!!");
		u_file->change_user_right(table_user.Find(str[1])->get_ID(), str[2]);
	}
	else{
		Catalog * ctl = static_cast<Catalog *>(file);
		if (!ctl->check_right(ID_current_user, "change rights", table_user.Find(ID_current_user)->get_groups()))
			throw exception("You don't have a right to change rights of this catalog!!");
		ctl->change_user_right(table_user.Find(str[1])->get_ID(), str[2]);
	}
	return string();
}

string FileSystem::GroupRightChange(vector<const string> str){//��� ����� ��� �������� � ����� ������� ���� ��������
	File * file;
	vector<const string> nullrights;
	if (str.size() < 3)
		throw exception("Wrong number of operands!!!");
	if (!(file = Find(str[0], nullrights)))
		throw exception("File or catalog was not found!!!");
	if (!table_group.Find(str[1]))
		throw exception("There are not this group in file system!!!");
	if (typeid(*file) == typeid(UsualFile)){
		UsualFile * u_file = static_cast<UsualFile*>(file);
		if (!u_file->check_right(ID_current_user, "change rights", table_user.Find(ID_current_user)->get_groups()))
			throw exception("You don't have a right to change rights of this file!!!");
		u_file->change_group_right(table_group.Find(str[1])->get_ID(), str[2]);
	}
	else{
		Catalog * ctl = static_cast<Catalog *>(file);
		if (!ctl->check_right(ID_current_user, "change rights", table_user.Find(ID_current_user)->get_groups()))
			throw exception("You don't have a right to change rights of this catalog!!");
		ctl->change_group_right(table_group.Find(str[1])->get_ID(), str[2]);
	}
	return string();
}

string FileSystem::CreateCommand(vector<const string> str){
	int i = 1, j, k, offset = 0;
	bool fl;
	Command * com;
	vector<Command *> commands;
	vector<int> vec;
	string s; 
	vector<const string> res = str;
	res.erase(res.begin());
	while (i < str.size()){
		res.erase(res.begin() + offset);
		if (str[i] == "create command")
			throw exception("You can't use command 'create command' in composite commands!!!");
		if (!(com = table_command.get_command(str[i++])))
			throw exception("Command was not found!!!");
		commands.push_back(com);
		for (j = 0; j < com->get_number_of_operands(); j++, offset++){
			fl = false;
			if (str[j + i][0] == '$'){
				for (k = 1; k < str[j + i].size(); k++){
					if ((int)(str[j + i][k]) < 49 || (int)(str[j + i][k]) > 57)
						throw exception("Incorrect data!!! Parametr should have form: '$' + 'number'!!!");
					s.push_back(str[j + i][k]);
				}
				for (k = 0; k < vec.size(); k++)
					if (vec[k] == stoi(s))
						fl = true;
				if (!fl)
					vec.push_back(stoi(s));
				s.clear();
			}
		}
		i += j;
	}
	for (k = 1; k <= vec.size(); k++){
		fl = false;
		for (i = 0; i < vec.size(); i++)
			if (vec[i] == k){
				fl = true;
				break;
			}
		if (!fl)
			throw exception("Incorrect data!!! Numbers of parametrs should be in order!!!");
	}
	for (i = 1; i < vec.size(); i++)
		if (vec[i - 1] > vec[i])
			throw exception("Incorrect data!!! Numbers of parametrs should be in order!!!");
	CompositeCommand * new_command = new CompositeCommand(commands, res, vec.size());
	table_command.Add(str[0], new_command);
	return string();
}

string CompositeCommand::Run(FileSystem & FS, vector<const string> operand){
	int  i, j;
	vector<const string> operand1;
	for (i = 0; i < commands.size(); i++){
		if (typeid(*(commands[i])) == typeid(CompositeCommand)){
			for (j = 0; j < commands[i]->get_number_of_operands(); j++)
				operand1.push_back(operand[j]);
			CompositeCommand * cmp = static_cast<CompositeCommand*>(commands[i]);
			commands[i]->Run(FS, cmp->Make_operand(operand1));
		}
		else
			commands[i]->Run(FS, operand);
		for (j = 0; j < commands[i]->get_number_of_operands(); j++)
			operand.erase(operand.begin());
	}
	return string();
}

vector<const string> CompositeCommand::Make_operand(vector<const string> str){
	vector<const string> result;
	string s;
	int i, j;
	for (i = 0; i < model.size(); i++)
		if (model[i].size() && model[i][0] == '$'){
			s = model[i];
			s.erase(s.begin());
			result.push_back(str[stoi(s) - 1]);
		}
		else
			result.push_back(model[i]);
	return result;
}

CompositeCommand::~CompositeCommand(){}

void Fwrite_str(const string & str, FILE * file){
	for (int i = 0; i < str.size(); i++)
		fwrite(&(str[i]), sizeof(char), 1, file);
}

void Fread_str(string & str, int len,  FILE * file){
	char simbol;
	str.clear();
	for (int i = 0; i < len; i++){
		fread(&simbol, sizeof(char), 1, file);
		str.push_back(simbol);
	}
}

void FileSystem::saveCatalogs(FILE * file, File * ptr)const{
	int i;
	Catalog * ctlg = static_cast<Catalog*>(ptr);
	map<const string, File*> v = ctlg->get_catalog();
	map<const string, File*>::iterator iter;
	for (iter = v.begin(); iter != v.end(); ++iter)
		if (typeid(*(iter->second)) == typeid(Catalog)){
			Catalog * ctl = static_cast<Catalog *>(iter->second);
			string adress = ctl->get_place();
			int len = adress.size();
			fwrite(&len, sizeof(int), 1, file);//����� ������
			Fwrite_str(adress, file);//�����
			len = iter->first.size();
			fwrite(&len, sizeof(int), 1, file);//����� ����� ��������
			Fwrite_str(iter->first, file);//��� ��������
			int number = ctl->get_ID_user();
			fwrite(&number, sizeof(int), 1, file);//������������ ������������
			number = ctl->get_ID_group();
			fwrite(&number, sizeof(int), 1, file);//������������� ������
			struct tm make_time = ctl->get_makingTime();
			fwrite(&make_time, sizeof(tm), 1, file);//����� �������� �����
			vector<AccessCatalogRights> user_rights = ctl->get_users_rights();
			map<const string, bool>::iterator iterator;
			for (i = 0; i < user_rights.size(); i++){
				fwrite(&(user_rights[i].ID_owner), sizeof(int), 1, file);//����� �������������
				for (iterator = user_rights[i].rights.begin(); iterator != user_rights[i].rights.end(); iterator++)
					fwrite(&(iterator->second), sizeof(bool), 1, file);
			}
			vector<AccessCatalogRights> group_rights = ctl->get_groups_rights();
			for (i = 0; i < group_rights.size(); i++){
				fwrite(&(group_rights[i].ID_owner), sizeof(int), 1, file);//����� �����
				for (iterator = group_rights[i].rights.begin(); iterator != group_rights[i].rights.end(); iterator++)
					fwrite(&(iterator->second), sizeof(bool), 1, file);
			}
			saveCatalogs(file, iter->second);
		}
}

void FileSystem::saveFiles(FILE * file, File * ptr)const{
	int i;
	Catalog * ctl = static_cast<Catalog*>(ptr);
	map<const string, File*> v = ctl->get_catalog();
	map<const string, File*>::iterator iter;
	for (iter = v.begin(); iter != v.end(); ++iter)
		if (typeid(*(iter->second)) == typeid(Catalog))
			saveFiles(file, iter->second);
		else{
			UsualFile * f = static_cast<UsualFile *>(iter->second);
			string adress = f->get_place();
			int len = adress.size();
			fwrite(&len, sizeof(int), 1, file);//����� ������
			Fwrite_str(adress, file);//�����
			len = iter->first.size();
			fwrite(&len, sizeof(int), 1, file);//����� ����� ��������
			fwrite(iter->first.c_str(), sizeof(char), len, file);//��� ��������
			int number = f->get_ID_user();
			fwrite(&number, sizeof(int), 1, file);//������������ ������������
			number = f->get_ID_group();
			fwrite(&number, sizeof(int), 1, file);//������������� ������
			struct tm make_time = f->get_makingTime();
			fwrite(&make_time, sizeof(tm), 1, file);//����� �������� �����
			len = f->get_data().size();
			fwrite(&len, sizeof(int), 1, file);//������ �����
			fwrite(f->get_data().c_str(), sizeof(char), len, file);//������
			vector<AccessFileRights> user_rights = f->get_users_rights();
			map<const string, bool>::iterator iterator;
			for (i = 0; i < user_rights.size(); i++){
				fwrite(&(user_rights[i].ID_owner), sizeof(int), 1, file);//����� �������������
				for (iterator = user_rights[i].rights.begin(); iterator != user_rights[i].rights.end(); iterator++)
					fwrite(&(iterator->second), sizeof(bool), 1, file);
			}
			vector<AccessFileRights> group_rights = f->get_groups_rights();
			for (i = 0; i < group_rights.size(); i++){
				fwrite(&(group_rights[i].ID_owner), sizeof(int), 1, file);//����� �����
				for (iterator = group_rights[i].rights.begin(); iterator != group_rights[i].rights.end(); iterator++)
					fwrite(&(iterator->second), sizeof(bool), 1, file);
			}
		}
}

void FileSystem::SaveInFile(const string & file_name){
	FILE * file;
	int len = 4, number, i, j;
	if (!(file = fopen(file_name.c_str(), "wb")))
		throw exception("File can't be opened!!!");
	fwrite(&len, sizeof(int), 1, file);//����� ����� ��������� ��������
	fwrite("root", sizeof(char), len, file);//��� ��������� ��������
	number = table_user.number();
	fwrite(&number, sizeof(int), 1, file);//���������� �������������
	number = table_group.number();
	fwrite(&number, sizeof(int), 1, file);//���������� �����
	number = 0;
	number = CatalogsNumber(root, number);
	fwrite(&number, sizeof(int), 1, file);
	saveCatalogs(file, root);//��������
	number = 0;
	number = FilesNumber(root, number);
	fwrite(&number, sizeof(int), 1, file);
	saveFiles(file, root);//�����
	Vector<User *> u_arr = table_user.get_arr();
	for (i = 1; i < table_user.number(); i++){
		number = u_arr[i]->get_ID();
		fwrite(&number, sizeof(int), 1, file);//�������������
		len = u_arr[i]->get_name().size();
		fwrite(&len, sizeof(int), 1, file);//����� �����
		Fwrite_str(u_arr[i]->get_name(), file);//���
		vector<int> groups = u_arr[i]->get_groups();
		number = groups.size();
		fwrite(&number, sizeof(int), 1, file);//���������� �����
		for (j = 0; j < groups.size(); j++)
			fwrite(&(groups[i]), sizeof(int), 1, file);//�������������� �����
	}
	vector<Group *> g_arr = table_group.get_arr();
	for (i = 1; i < table_group.number(); i++){
		number = g_arr[i]->get_ID();
		fwrite(&number, sizeof(int), 1, file);//�������������
		len = g_arr[i]->get_name().size();
		fwrite(&len, sizeof(int), 1, file);//����� �����
		Fwrite_str(g_arr[i]->get_name(), file);//���
		vector<int> users = g_arr[i]->get_users();
		number = users.size();
		fwrite(&number, sizeof(int), 1, file);//���������� �������������
		for (j = 0; j < users.size(); j++)
			fwrite(&(users[i]), sizeof(int), 1, file);//�������������� �������������
	}
	
	number = table_command.number() - 17;
	fwrite(&number, sizeof(int), 1, file);//���������� ��������� ������
	map<const string, Command *>::iterator iterator, iter;
	map<const string, Command *> map = table_command.get_arr();
	for (iterator = map.begin(); iterator != map.end(); ++iterator)
		if (typeid(*(iterator->second)) == typeid(CompositeCommand)){
			len = iterator->first.size();
			fwrite(&len, sizeof(int), 1, file);//����� �����
			Fwrite_str(iterator->first, file);//���
			number = iterator->second->get_number_of_operands();
			fwrite(&number, sizeof(int), 1, file);//���������� ������������� ���������
			CompositeCommand * cmp = static_cast<CompositeCommand *>(iterator->second);
			number = cmp->get_model().size();
			fwrite(&number, sizeof(int), 1, file);//���������� ���������
			for (i = 0; i < cmp->get_model().size(); i++){
				number = cmp->get_model()[i].size();
				fwrite(&number, sizeof(int), 1, file);//����� ��������
				Fwrite_str(cmp->get_model()[i], file);//�������
			}
		}
	for (iterator = map.begin(); iterator != map.end(); ++iterator)
		if (typeid(*(iterator->second)) == typeid(CompositeCommand)){
			CompositeCommand * cmp = static_cast<CompositeCommand *>(iterator->second);
			vector<Command *> commands = cmp->get_commands();
			number = commands.size();
			fwrite(&number, sizeof(int), 1, file);//���������� ���������� ������
			for (i = 0; i < commands.size(); i++){
				for (iter = map.begin(); iter != map.end(); ++iter)
					if (iter->second == commands[i])
						break;
				number = iter->first.size();
				fwrite(&number, sizeof(int), 1, file);//����� �����
				Fwrite_str(iter->first, file);//���
			}
		}
	fclose(file);
	return;
}

void FileSystem::ReadFromFile(const string & file_name){
	FILE * file;
	int len, number, number_users, number_groups, number_commands, i, j, ID_user, ID_group;
	string name, adress, data;
	AccessCatalogRights c_r(0, 0);
	AccessFileRights f_r(0, 0);
	map<const string, bool>::iterator iterator;
	struct tm make_time;
	Start_work_with_user("Administrator");
	if (!(file = fopen(file_name.c_str(), "rb")))
		throw exception("File can't be opened!!!");
	fread(&len, sizeof(int), 1, file);
	Fread_str(root_name, len, file);
	fread(&number_users, sizeof(int), 1, file);
	fread(&number_groups, sizeof(int), 1, file);
	fread(&number, sizeof(int), 1, file);
	for (j = 0; j < number; j++){
		fread(&len, sizeof(int), 1, file);//����� ������ ��������
		Fread_str(adress, len, file);//����� ��������
		fread(&len, sizeof(int), 1, file);//����� ����� ��������
		Fread_str(name, len, file);//��� ��������
		fread(&ID_user, sizeof(int), 1, file);//������������ ������������
		fread(&ID_group, sizeof(int), 1, file);//������������� ������
		fread(&make_time, sizeof(tm), 1, file);//����� �������� �����
		vector<AccessCatalogRights> user_rights;
		for (i = 0; i < number_users; i++){
			fread(&c_r.ID_owner, sizeof(int), 1, file);//����� �������������
			for (iterator = c_r.rights.begin(); iterator != c_r.rights.end(); iterator++)
				fread(&(iterator->second), sizeof(bool), 1, file);
			user_rights.push_back(c_r);
		}
		vector<AccessCatalogRights> group_rights;
		for (i = 0; i < number_groups; i++){
			fread(&c_r.ID_owner, sizeof(int), 1, file);//����� �������������
			for (iterator = c_r.rights.begin(); iterator != c_r.rights.end(); iterator++)
				fread(&(iterator->second), sizeof(bool), 1, file);
			group_rights.push_back(c_r);
		}
		Catalog * ctl = new Catalog(ID_user, ID_group, user_rights, group_rights);
		ctl->set_makingTime(make_time);
		ctl->set_place(adress);
		vector<const string> nullrights;
		Catalog * ctlg = static_cast<Catalog *>(Find(adress, nullrights));
		ctlg->add_to_catalog(name, ctl);
	}
	fread(&number, sizeof(int), 1, file);
	for (j = 0; j < number; j++){
		fread(&len, sizeof(int), 1, file);//����� ������
		Fread_str(adress, len, file);//�����
		fread(&len, sizeof(int), 1, file);//����� ����� 
		Fread_str(name, len, file);//��� 
		fread(&ID_user, sizeof(int), 1, file);//������������ ������������
		fread(&ID_group, sizeof(int), 1, file);//������������� ������
		fread(&make_time, sizeof(tm), 1, file);//����� �������� �����
		fread(&len, sizeof(int), 1, file);//������ �����
		Fread_str(data, len, file);//������
		vector<AccessFileRights> user_rights;
		for (i = 0; i < number_users; i++){
			fread(&f_r.ID_owner, sizeof(int), 1, file);//����� �������������
			for (iterator = f_r.rights.begin(); iterator != f_r.rights.end(); iterator++)
				fread(&(iterator->second), sizeof(bool), 1, file);
			user_rights.push_back(f_r);
		}
		vector<AccessFileRights> group_rights;
		for (i = 0; i < number_groups; i++){
			fread(&f_r.ID_owner, sizeof(int), 1, file);//����� �������������
			for (iterator = f_r.rights.begin(); iterator != f_r.rights.end(); iterator++)
				fread(&(iterator->second), sizeof(bool), 1, file);
			group_rights.push_back(f_r);
		}
		UsualFile * f = new UsualFile(ID_user, ID_group, user_rights, group_rights);
		f->set_makingTime(make_time);
		f->Write(data, 1);
		f->set_place(adress);
		vector<const string> nullrights;
		Catalog * ctlg = static_cast<Catalog *>(Find(adress, nullrights));
		ctlg->add_to_catalog(name, f);
	}
	for (i = 0; i < number_users - 1; i++){
		fread(&number, sizeof(int), 1, file); //������������� ������������
		fread(&len, sizeof(int), 1, file);//����� �����
		Fread_str(name, len, file);//���
		User user(name);
		user.setID(number);
		fread(&number, sizeof(int), 1, file);//���������� �����
		int n;
		for (j = 0; j < number; j++){
			fread(&n, sizeof(int), 1, file);//������������� ������
			user.Add(n);
		}
		table_user.Add(&user);
	}
	for (i = 0; i < number_groups - 1; i++){
		fread(&number, sizeof(int), 1, file);//������������� ������
		fread(&len, sizeof(int), 1, file);//����� �����
		Fread_str(name, len, file);//���
		Group group(name);
		group.setID(number);
		fread(&number, sizeof(int), 1, file);//���������� �������������
		int n;
		for (j = 0; j < number; j++){
			fread(&n, sizeof(int), 1, file);//������������� ������������
			group.Add(n);
		}
		table_group.Add(&group);
	}
	fread(&number_commands, sizeof(int), 1, file);//���������� ��������� ������
	for (i = 0; i < number_commands; i++){
		fread(&len, sizeof(int), 1, file);//����� �����
		Fread_str(name, len, file);//���
		fread(&number, sizeof(int), 1, file);//���������� ������������� ���������
		string s;
		int n;
		fread(&n, sizeof(int), 1, file);//���������� ���������
		vector<const string> model;
		for (j = 0; j < n; j++){
			fread(&len, sizeof(int), 1, file);//����� ��������
			Fread_str(s, len, file);//�������
			model.push_back(s);
		}
		CompositeCommand cmp(number, model);
		table_command.Add(name, cmp.clone());
	}
	map<const string, Command *>::iterator iter;
	for (iter = table_command.begin(); iter != table_command.end(); ++iter)
		if (typeid(*(iter->second)) == typeid(CompositeCommand)){
			CompositeCommand * cmp = static_cast<CompositeCommand *>(iter->second);
			fread(&number, sizeof(int), 1, file);//���������� ���������� ������
			for (i = 0; i < number; i++){
				fread(&len, sizeof(int), 1, file);//����� �����
				Fread_str(name, len, file);//���
				cmp->Add_command(table_command.Find(name));
			}
		}
	fclose(file);
	Finish_work_with_user();
}

int FileSystem::FilesNumber(File * ptr, int number){
	Catalog * ctlg = static_cast<Catalog*>(ptr);
	map<const string, File*> v = ctlg->get_catalog();
	map<const string, File*>::iterator iter;
	for (iter = v.begin(); iter != v.end(); ++iter)
		if (typeid(*(iter->second)) == typeid(Catalog))
			number += FilesNumber(iter->second, 0);
		else
			number++;
	return number;
}

int FileSystem::CatalogsNumber(File * ptr, int number){
	Catalog * ctlg = static_cast<Catalog*>(ptr);
	map<const string, File*> v = ctlg->get_catalog();
	map<const string, File*>::iterator iter;
	for (iter = v.begin(); iter != v.end(); ++iter)
		if (typeid(*(iter->second)) == typeid(Catalog))
			number += CatalogsNumber(iter->second, 0) + 1;
	return number;
}

TableCommand::TableCommand(const TableCommand & item){
	map<const string, Command *>::const_iterator ptr;
	for (ptr = item.arr.begin(); ptr != item.arr.end(); ++ptr){
		if (typeid(*(ptr->second)) == typeid(CompositeCommand)){
			CompositeCommand * cmp = static_cast<CompositeCommand *>(ptr->second);
			CompositeCommand * c = new CompositeCommand(*cmp);
			arr.insert(make_pair(ptr->first, c));
		}
		else{
			SimpleCommand * cmp = static_cast<SimpleCommand *>(ptr->second);
			SimpleCommand * c = new SimpleCommand(*cmp);
			arr.insert(make_pair(ptr->first, c));
		}
	}
}