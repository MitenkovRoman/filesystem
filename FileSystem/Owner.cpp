#include "stdafx.h"
#include "Owner.h"

TableUser::~TableUser(){
	for (int i = 0; i < arr.size(); i++){
		delete arr[i];
		arr[i] = nullptr;
	}
}

TableGroup::~TableGroup(){
	for (int i = 0; i < arr.size(); i++){
		delete arr[i];
		arr[i] = nullptr;
	}
}

User * TableUser::Find(const string & uname)const{
	for (int i = 0; i < arr.size(); i++)
		if (arr[i]->get_name() == uname)
			return arr[i];
	return nullptr;
}

Group * TableGroup::Find(const string & gname)const{
	for (int i = 0; i < arr.size(); i++)
		if (arr[i]->get_name() == gname)
			return arr[i];
	return nullptr;
}

User * TableUser::Find(int ID)const{
	for (int i = 0; i < arr.size(); i++)
		if (arr[i]->get_ID() == ID)
			return arr[i];
	return nullptr;
}

Group * TableGroup::Find(int ID)const{
	for (int i = 0; i < arr.size(); i++)
		if (arr[i]->get_ID() == ID)
			return arr[i];
	return nullptr;
}

void User::Delete(int id){
	vector<int>::iterator ptr;
	for (ptr = groups.begin(); ptr != groups.end(); ++ptr)
		if (id == (*ptr)){
			groups.erase(ptr);
			return;
		}
}

void Group::Delete(int id){
	vector<int>::iterator ptr;
	for (ptr = users.begin(); ptr != users.end(); ++ptr)
		if (id == (*ptr)){
			users.erase(ptr);
			return;
		}
}

void TableUser::Delete(const string & str){
	Vector<User*>::Iterator ptr;
	for (ptr = arr.begin(); ptr != arr.end(); ++ptr)
		if ((*ptr)->get_name() == str){
			arr.erase(ptr);
			return;
		}
}

void TableGroup::Delete(const string & str){
	vector<Group*>::iterator ptr;
	for (ptr = arr.begin(); ptr != arr.end(); ++ptr)
		if ((*ptr)->get_name() == str){
			arr.erase(ptr);
			return;
		}
}