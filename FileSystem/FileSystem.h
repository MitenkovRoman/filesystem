#pragma once
#include "File.h"
#include "Owner.h"
#include <vector>

using namespace std;

class FileSystem;

/**����������� ������� ����� �������, ����������� �������� ��������*/
class Command{
protected:
	int number_of_operands;
public:
	int get_number_of_operands()const{ return number_of_operands; }
	virtual string Run(FileSystem & FS, vector<const string> parametrs) = 0;///<������� ���������� ������� � ����������� parametrs � �������� ������� FS
	virtual ~Command(){}
};

class SimpleCommand : public Command{
private:
	string(FileSystem::*operation)(vector<const string>); ///<�������, ����������� �������
public:
	SimpleCommand(string(FileSystem::*func)(vector<const string>), int n){ operation = func; Command::number_of_operands = n; }
	~SimpleCommand(){}
	string Run(FileSystem & FS, vector<const string> operand){ return (FS.*operation)(operand); }///<������������� ����� ����������� ������� ������������ ������ Command
};

class CompositeCommand : public Command{
private:
	vector<Command *> commands;
	vector<const string> model;
public:
	CompositeCommand(int n, vector<const string> m){ number_of_operands = n; model = m; }
	CompositeCommand(vector<Command*> com, vector<const string> str_model, int n) : commands(com), model(str_model){ Command::number_of_operands = n; }
	virtual ~CompositeCommand();
	Command * clone()const{ return new CompositeCommand(*this); }
	void Add_command(Command * c){ commands.push_back(c); }
	vector<Command *> get_commands()const{ return commands; }
	vector <const string> get_model()const{ return model; }
	vector<const string> Make_operand(vector<const string> str);
	string Run(FileSystem & FS, vector<const string> operand);
};

class TableCommand{
private:
	map<const string, Command *> arr; ///<������� ������, �������������� � ���� map
public:
	TableCommand(){}
	TableCommand(const TableCommand &);
	virtual ~TableCommand();
	int number()const{ return arr.size(); }
	map<const string, Command *>::iterator begin(){ return arr.begin(); }
	map<const string, Command *>::iterator end(){ return arr.end(); }
	map<const string, Command *> get_arr(){ return arr; }
	Command * get_command(const string & str); ///<������� ��������� ������� �� ����� �� ������� ������
	void Add(const string & command_name, Command * command){ arr.insert(make_pair(command_name, command)); } ///<������� ���������� ������� � ������� ������
	void Delete(const string & str){ delete arr.find(str)->second; arr.erase(str); };///<������� �������� ������� �� ������� ������
	Command * Find(const string & str){ if (arr.find(str) != arr.end()) return arr.find(str)->second; return nullptr; }
};

class FileSystem{
private:
	Catalog * root; ///<��������� �� �������� �������
	TableUser table_user;///<������� �������������
	TableGroup table_group;///<������� �����
	TableCommand table_command;///<������� �����������
	string root_name;///<��� ��������� ��������
	int ID_current_user;///<������������� �������� ������������ �������� �������
	void print(ostream & c, File * ptr, int level)const;
	void add_user(File *, User *);
	void delete_user(File *, User *);
	void add_group(File *, Group *);
	void delete_group(File *, Group *);
	void changePlaces(File *, const string &, const string &);
	void saveCatalogs(FILE * file, File * ptr)const;
	void saveFiles(FILE * file, File * ptr)const;
public:
	FileSystem();
	FileSystem(FileSystem &);
	FileSystem(FileSystem &&);
	FileSystem & operator = (const FileSystem &);
	FileSystem & operator = (FileSystem &&);
	virtual ~FileSystem(){ delete root; }
	TableCommand get_table_command()const{ return table_command; }
	int groups()const{ return table_group.number(); }///<������� ��������� ���������� ����� � �������� �������
	int users()const{ return table_user.number(); }///<������� ��������� ���������� ������������� � �������� �������
	const string & get_root_name()const{ return root_name; }///<������� ��������� ����� ��������� ��������
	const Catalog * get_root()const{ return root; }///<������� ��������� ��������� �� �������� �������
	void set_root_name(const string & s){ root_name = s; }///<������� ��������� ����� ��������� ��������
	int FilesNumber(File *, int);
	int CatalogsNumber(File *, int);
	File * Find(const string & fname, vector<const string> rights);///<������� ������ ����� ��� �������� � ��������� ��������������� ���� ������� rights
	Catalog * Find_place(const string &, vector<const string> rights);///<������� ������ ��������, � ������� ��������� ������ ���� ��� ������� � ��������� ��������������� ���� ������� rights
	User * Find_user(const string & str){ return table_user.Find(str); }///<������� ������ ������������ �� ��� �����
	Group * Find_group(const string & str){ return table_group.Find(str); }///<������� ������ ������ �� � �����
	Command * Find_command(const string & str){ return table_command.Find(str); }
	void Start_work_with_user(const string &);///<������� �������������� ������ ������ � ������ �������������
	void Add_new_user(const string &);///<������� ���������� ������ ������������ � �������� �������
	string Delete_user(vector<const string> str);///<������� �������� ������������ �� �������� �������
	void Add_new_group(const string &);///<������� ���������� ����� ������ � �������� �������
	string Delete_group(vector<const string> str);///<������� �������� ������ �� �������� �������
	string Add_user_in_group(vector<const string>);///<������� ���������� ������������ � ������
	string Delete_user_from_group(vector<const string>);///<������� �������� ������������ �� ������
	void Finish_work_with_user(){ ID_current_user = -1; }///<������� ���������� ������ ������ � ������� �������������
	string Run(const string & str, vector<const string> operand);///<������� ������� ���������� str c ����������� operand
	string CreateCatalog(vector<const string> str);///<������� �������� �������� str[0] � �������� str[1]
	string CreateCommand(vector<const string> str);
	string DeleteCommand(vector<const string> str){ table_command.Delete(str[0]); return string(); }
	string CreateFile(vector<const string> str);///<������� �������� ����� str[0] � �������� str[1]
	string Copy(vector<const string> str);///<������� ����������� ����� ��� �������� str[0] � ������� str[1]
	string Move(vector<const string> str);///<������� ����������� ����� ��� �������� str[0] � ������� str[1]
	string Delete(vector<const string> str);///<������� �������� ����� ��� �������� str[0]
	string Rename(vector<const string> str);///<������� �������������� ����� ��� �������� str[0] (����� ��� - str[1])
	string Show(vector<const string> str){ cout << *this << endl; return string(); }///<������� ����������� ������ �������� �������
	string Write(vector<const string> str);///<������� ������ � ���� str[0] ������ str[1] � ������� str[2]
	string Read(vector<const string> str);///<������� ������ �� ����� str[0] ����� �������� ������� str[1] c ������� str[2]
	string UserRightChange(vector<const string> str);///<������� ��������� ����� ������� str[2] � ����� ��� �������� str[0] ��� ������������ str[1]
	string GroupRightChange(vector<const string> str);///<������� ��������� ����� ������� str[2] � ����� ��� �������� str[0] ��� ������ str[1]
	friend ostream & operator << (ostream &, const FileSystem &);///<������������� �������� ������ � ����� ���������� � �������� �������
	void SaveInFile(const string &);
	void ReadFromFile(const string &);
};
