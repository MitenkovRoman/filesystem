#include "stdafx.h"
#include "File.h"
#include "Owner.h"
#include <typeinfo>
#include "FileSystem.h"


UsualFile::UsualFile(int id_user, int id_group, vector<AccessFileRights> & u_rights, vector<AccessFileRights> & g_rights) :
size(0), lock_read(false), lock_write(false){
	group_rights = g_rights;
	user_rights = u_rights;
	File::ID_user = id_user;
	File::ID_group = id_group;
	adress = 0; //����������� ������ ���������� ����� �� �����
	time_t time_now = time(NULL);
	localtime_s(&makingTime, &time_now);
}

Catalog::Catalog(int id_user, int id_group, vector<AccessCatalogRights> & u_rights, vector<AccessCatalogRights> & g_rights){
	group_rights = g_rights;
	user_rights = u_rights;
	File::ID_user = id_user;
	File::ID_group = id_group;
	adress = 0; //����������� ������ ���������� ����� �� �����
	time_t time_now = time(NULL);
	localtime_s(&makingTime, &time_now);
}

AccessFileRights::AccessFileRights(int id, bool r){
	ID_owner = id;
	rights.insert(make_pair("write", r));
	rights.insert(make_pair("read", r));
	rights.insert(make_pair("change meta data", r));
	rights.insert(make_pair("change rights", r));
	rights.insert(make_pair("change", r));
}

AccessCatalogRights::AccessCatalogRights(int id, bool r){
	ID_owner = id;
	rights.insert(make_pair("write", r));
	rights.insert(make_pair("read", r));
	rights.insert(make_pair("change meta data", r));
	rights.insert(make_pair("change rights", r));
	rights.insert(make_pair("change", r));
	rights.insert(make_pair("pass", r));
}

void Catalog::add_to_catalog(string fname, File * file){
	if (catalog.find(fname) != catalog.end())
		throw exception("File with this name in this catalog already exists!!!");
	catalog.insert(make_pair(fname, file->clone()));
}

void Catalog::delete_from_catalog(string fname){
	map<const string, File *>::iterator p;
	if ((p = catalog.find(fname)) == catalog.end())
		throw exception("File was not found!!!");
	delete p->second;
	catalog.erase(fname);
}

void clear_catalog(File * ptr){
	if (typeid(*ptr) == typeid(Catalog)){
		Catalog * ctl = static_cast<Catalog*>(ptr);
		map<const string, File*>::iterator ptr, p;
		if (ctl->catalog.size()){
			for (ptr = ctl->catalog.begin(); ptr != ctl->catalog.end();){
				clear_catalog(ptr->second);
				p = ptr;
				++ptr;
				delete p->second;
				ctl->catalog.erase(p);
			}
		}
	}
}

void Catalog::clear(){
	clear_catalog(this);
}

void Catalog::Rename_catalog(const string & old_name, const string & new_name){
	bool fl = false;
	checkName(old_name);
	checkName(new_name);
	map<const string, File *>::iterator ptr;
	if ((ptr = catalog.find(old_name)) == catalog.end())
		throw exception("File with this name was not found!!!");
	add_to_catalog(new_name, ptr->second);
	delete_from_catalog(old_name);
}

File * Catalog::findItem(const string & name){
	if (catalog.end() == catalog.find(name))
		return nullptr;
	return catalog.find(name)->second;
}

void checkName(const string & n){
	if (string::npos != n.find("\\"))
		throw exception("Incorrect name of file or catalog!!!");
	if (string::npos != n.find("*"))
		throw exception("Incorrect name of file or catalog!!! Simbol * can't be in names of files or catalogs!!!");
}

string makeName(const string & n){//���������� ��� ����� �� ������
	string str;
	int i, j;
	if (string::npos == n.find("\\"))
		return n;
	for (i = n.size(); i >= 0 && n[i] != '\\'; i--);
	for (j = i + 1; j < n.size(); j++)
		str.push_back(n[j]);
	return str;
}

void UsualFile::Write(const string & str, int position){
	if (position <= 0)
		throw exception("Wrong position!!!");
	if (abs(position) >= size){
		data += str;
		size += str.size();
	}
	else{
		if ((size - position + 1) < str.size())
			size += (str.size() - size + position - 1);
		data.replace(position - 1, str.size(), str);
	}
}

string UsualFile::Read(int position, int number){
	string s;
	if (position <= 0 || position >= size)
		throw exception("Wrong position!!!");
	for (int i = position - 1; i < size && i - position + 1 < number; i++)
		s += data[i];
	return s;
}

File * clone_catalog(const File * ptr, File * res){
	int i, j;
	if (typeid(*ptr) == typeid(Catalog)){
		map<const string, File*> v = static_cast<const Catalog*>(ptr)->get_catalog();
		res = new Catalog(*(static_cast<const Catalog *>(ptr)));
		Catalog * res1 = static_cast<Catalog *>(res);
		map<const string, File*>::iterator ptr1, ptr2, ptr3;
		if (v.size())
			for (ptr1 = v.begin(), ptr2 = res1->catalog.begin(); ptr1 != v.end(); ++ptr1, ++ptr2){
				if (typeid(*(ptr1->second)) == typeid(Catalog))
					ptr2->second = new Catalog(*(static_cast<Catalog *>(ptr1->second)));
				else
					ptr2->second = ptr1->second->clone();
				ptr2->second = clone_catalog(ptr1->second, ptr2->second);
			}
	}
	else{
		res = new UsualFile(*(static_cast<const UsualFile *>(ptr)));
	}
	return res;
}

Catalog * Catalog::clone()const{
	File * result = nullptr;
	result = clone_catalog(this, result);
	return static_cast<Catalog*>(result);
}

const string & Catalog::get_file_name(int i)const{
	int j = 0;
	if (i >= catalog.size())
		throw exception("Incorrect index!!!");
	map<const string, File *>::const_iterator ptr;
	for (ptr = catalog.begin(); j != i; ++ptr, j++);
	return ptr->first;
}

void UsualFile::add_user(int id, bool b){
	AccessFileRights rights(id, false);
	if (b){
		rights.rights.find("read")->second = true;
		rights.rights.find("write")->second = true;
		rights.rights.find("change")->second = true;
	}
	user_rights.push_back(rights);
}

void UsualFile::delete_user(int id_user){
	vector<AccessFileRights>::iterator ptr;
	for (ptr = user_rights.begin(); ptr != user_rights.end(); ++ptr)
		if (ptr->ID_owner == id_user){
			user_rights.erase(ptr);
			return;
		}
}

void UsualFile::add_group(int id, bool b){
	AccessFileRights rights(id, false);
	if (b){
		rights.rights.find("read")->second = true;
		rights.rights.find("write")->second = true;
		rights.rights.find("change")->second = true;
	}
	group_rights.push_back(rights);
}

void UsualFile::delete_group(int id_group){
	vector<AccessFileRights>::iterator ptr;
	for (ptr = group_rights.begin(); ptr != group_rights.end(); ++ptr)
		if (ptr->ID_owner == id_group){
			group_rights.erase(ptr);
			return;
		}
}

void Catalog::add_user(int id, bool b){
	AccessCatalogRights rights(id, b);
	if (b){
		rights.rights.find("read")->second = true;
		rights.rights.find("write")->second = true;
		rights.rights.find("pass")->second = true;
		rights.rights.find("change")->second = true;
	}
	user_rights.push_back(rights);
}

void Catalog::delete_user(int id_user){
	vector<AccessCatalogRights>::iterator ptr;
	for (ptr = user_rights.begin(); ptr != user_rights.end(); ++ptr)
		if (ptr->ID_owner == id_user){
			user_rights.erase(ptr);
			return;
		}
}

void Catalog::add_group(int id, bool b){
	AccessCatalogRights rights(id, false);
	if (b){
		rights.rights.find("read")->second = true;
		rights.rights.find("write")->second = true;
		rights.rights.find("change")->second = true;
		rights.rights.find("pass")->second = true;
	}
	group_rights.push_back(rights);
}

void Catalog::delete_group(int id_group){
	vector<AccessCatalogRights>::iterator ptr;
	for (ptr = group_rights.begin(); ptr != group_rights.end(); ++ptr)
		if (ptr->ID_owner == id_group){
			group_rights.erase(ptr);
			return;
		}
}

AccessFileRights UsualFile::get_user_rights(int id){
	for (int i = 0; i < user_rights.size(); i++)
		if (id == user_rights[i].ID_owner)
			return user_rights[i];
	throw exception("User was not found!!!");
}

AccessCatalogRights Catalog::get_user_rights(int id){
	for (int i = 0; i < user_rights.size(); i++)
		if (id == user_rights[i].ID_owner)
			return user_rights[i];
	throw exception("User was not found!!!");
}

void UsualFile::change_user_right(int id, const string & right){
	for (int i = 0; i < user_rights.size(); i++)
		if (id == user_rights[i].ID_owner)
			if (user_rights[i].rights.find(right) != user_rights[i].rights.end())
				user_rights[i].rights.find(right)->second = !user_rights[i].rights.find(right)->second;
			else
				throw exception("There are not this right in list of right for file!!!");
}

void UsualFile::change_group_right(int id_group, const string & right){
	for (int i = 0; i < group_rights.size(); i++)
		if (id_group == user_rights[i].ID_owner)
			if (group_rights[i].rights.find(right) != group_rights[i].rights.end())
				group_rights[i].rights.find(right)->second = !group_rights[i].rights.find(right)->second;
			else
				throw exception("There are not this right in list of right for file!!!");
}

void Catalog::change_user_right(int id, const string & right){
	for (int i = 0; i < user_rights.size(); i++)
		if (id == user_rights[i].ID_owner)
			if (user_rights[i].rights.find(right) != user_rights[i].rights.end())
				user_rights[i].rights.find(right)->second = !user_rights[i].rights.find(right)->second;
			else
				throw exception("There are not this right in list of right for catalog!!!");
}

void Catalog::change_group_right(int id, const string & right){
	for (int i = 0; i < group_rights.size(); i++)
		if (id == group_rights[i].ID_owner)
			if (group_rights[i].rights.find(right) != group_rights[i].rights.end())
				group_rights[i].rights.find(right)->second = !group_rights[i].rights.find(right)->second;
			else
				throw exception("There are not this right in list of right for catalog!!!");
}

bool UsualFile::check_right(int id_user, const string & right, vector<int> groups)const{
	int i, j, k;
	for (i = 0; i < user_rights.size(); i++)
		if (id_user == user_rights[i].ID_owner)
			break;
	if (user_rights[i].rights.find(right)->second)
		return true;
	for (j = 0; j < groups.size(); j++){
		for (k = 0; k < group_rights.size(); k++)
			if (groups[j] == group_rights[k].ID_owner)
				break;
		if (group_rights[k].rights.find(right)->second)
			return true;
	}
	return false;
}

bool Catalog::check_right(int id_user, const string & right, vector<int> groups)const{
	int i, j, k;
	for (i = 0; i < user_rights.size(); i++)
		if (id_user == user_rights[i].ID_owner)
			break;
	if (user_rights[i].rights.find(right)->second)
		return true;
	for (j = 0; j < groups.size(); j++){
		for (k = 0; k < group_rights.size(); k++)
			if (groups[j] == group_rights[k].ID_owner)
				break;
		if (group_rights[k].rights.find(right)->second)
			return true;
	}
	return false;
}