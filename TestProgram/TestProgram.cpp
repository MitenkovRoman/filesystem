#include "stdafx.h"
#include "..\FileSystem\FileSystem.h"
#include <iostream>
#include <vector>
#include <sstream>

using namespace std;
const vector<const string> menu = { "0. Exit", "1. Log on", "2. Make new user", "3. Make new group" };
vector<const string> users = { "0. Back\n Choose user:", "1. Administrator" };
vector<const string> groups = { "1. Administrators" };
vector<const string> new_commands;
const vector<const string> menu2 = { "0. Finish work", "1. Create file", "2. Create catalog", "3. Delete file or catalog", "4. Rename file or catalog",
"5. Show file system", "6. Move file or catalog", "7. Copy file or catalog", "8. Write to file", "9. Read file", "10. Change access right for user",
"11. Change access rights for group", "12. Delete user", "13. Delete group", "14. Add user in group (only for Administrator)",
"15. Delete user from group (only for Administrator)", "16. Create composite command", "17. Run composite command", "18. Delete composite command" };

int getNum(int &a){// 0-�������, 1-����� �����, -1-������������ ����
	cin >> a;
	if (!cin.good()){
		if (cin.eof()){
			cout << "End of file!!!" << endl;
			return 1;
		}
		cout << "Incorrect data!!!" << endl;
		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
		return -1;
	}
	return 0;
}

int dialog(const vector<const string> str)
{
	char *errmsg = "";
	int rc;
	int i, n;

	do{
		puts(errmsg);
		errmsg = "You are wrong. Repeate, please!";

		// ����� ������ �����������
		for (i = 0; i < str.size(); ++i)
			cout << str[i] << endl;
		cout << "Make your choice: --> ";

		n = getNum(rc); // ���� ������ ������������
		if (n == 1) // ����� ����� - ����� ������
			return 0;
	} while (n == -1 || rc < 0 || rc >= str.size());

	cin.clear();
	cin.ignore(cin.rdbuf()->in_avail()); // �������, ��� �������������, ��� ���������� ������� �� ������ �����

	return rc;
}

int dataCreateFile(vector<const string> & data){
	string buf;
	cout << "Enter name of new file: ";
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	cout << "Enter adress of catalog, where you want to create file: ";
	buf.clear();
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	cout << "Enter name of own group: ";
	buf.clear();
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	return 1;
}

int dataCreateCatalog(vector<const string> & data){
	string buf;
	cout << "Enter name of new catalog: ";
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	cout << "Enter adress of catalog, where you want to create new catalog: ";
	buf.clear();
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	cout << "Enter name of own group: ";
	buf.clear();
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	return 1;
}

int dataRename(vector<const string> & data){
	string buf;
	cout << "Enter adress of file or catalog, whose name you want to change: ";
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	cout << "Enter new name of this file or catalog: ";
	buf.clear();
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	return 1;
}

int dataShowFileSystem(vector<const string> & data){ return 1; }

int dataDelete(vector<const string> & data){
	string buf;
	cout << "Enter adress of file or catalog you want to delete: ";
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	return 1;
}

int dataMove(vector<const string> & data){
	string buf;
	cout << "Enter adress of file or catalog you want to replace: ";
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	cout << "Enter adress of catalog, where you want to move this file or catalog : ";
	buf.clear();
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	return 1;
}

int dataCopy(vector<const string> & data){
	string buf;
	cout << "Enter adress of file or catalog you want to copy: ";
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	cout << "Enter adress of catalog, where you want to copy this file or catalog : ";
	buf.clear();
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	return 1;
}


int dataWrite(vector<const string> & data){
	string buf;
	cout << "Enter adress of file: ";
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	cout << "Enter data you want to write to file: ";
	buf.clear();
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	cout << "Enter the position in file to writing: ";
	buf.clear();
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	return 1;
}

int dataRead(vector<const string> & data){
	string buf;
	cout << "Enter adress of file: ";
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	cout << "Enter the position in file to reading: ";
	buf.clear();
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	cout << "Enter the number of simbols in file you want to read: ";
	buf.clear();
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	return 1;
}

int dataChangeUserRight(vector<const string> & data){
	string buf;
	cout << "Enter adress of file: ";
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	cout << "Enter name of user: ";
	buf.clear();
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	cout << "Enter right you want to change: ";
	buf.clear();
	cin.ignore(cin.rdbuf()->in_avail());
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	return 1;
}

int dataChangeGroupRight(vector<const string> & data){
	string buf;
	cout << "Enter adress of file: ";
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	cout << "Enter name of group: ";
	buf.clear();
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	cout << "Enter right you want to change: ";
	buf.clear();
	cin.ignore(cin.rdbuf()->in_avail());
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	return 1;
}


int dataAddUserInGroup(vector<const string> & data){
	string buf;
	cout << "Enter name of user: ";
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	cout << "Enter name of group: ";
	buf.clear();
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	return 1;
}

int dataDeleteUserFromGroup(vector<const string> & data){
	string buf;
	cout << "Enter name of user: ";
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	cout << "Enter name of group: ";
	buf.clear();
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	return 1;
}

int dataDeleteUser(vector<const string> & data){
	string buf;
	int i, j;
	cout << "Enter name of the user you want to delete: ";
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	return 1;
}

int dataDeleteGroup(vector<const string> & data){
	string buf;
	cout << "Enter name of the group you want to delete: ";
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	return 1;
}

int dataCompositeCommand(FileSystem & FS, const string & name, vector<const string> & data){
	string buf;
	int number, i;
	if (!FS.get_table_command().Find(name) || typeid(*(FS.get_table_command().Find(name))) != typeid(CompositeCommand)){
		cout << "Command was not found!!!" << endl;
		return 1;
	}
	try{
		if (FS.Find_command(name))
			number = FS.Find_command(name)->get_number_of_operands();
		for (i = 1; i <= number; i++){
			cout << "Enter meaning of $" << i << ": ";
			buf.clear();
			getline(cin, buf, cin.widen('\n'));
			data.push_back(buf);
		}
	}
	catch (exception & msg){
		cout << msg.what() << endl;
	}
	return 1;
}

int CreateFile(FileSystem & FS){
	vector<const string> data;
	dataCreateFile(data);
	try{
		FS.Run("create file", data);
		cout << "File was created!!!" << endl;
	}
	catch (exception & msg){
		cout << msg.what() << endl;
	}
	return 1;
}

int CreateCatalog(FileSystem & FS){
	vector<const string> data;
	dataCreateCatalog(data);
	try{
		FS.Run("create catalog", data);
		cout << "Catalog was created!!!" << endl;
	}
	catch (exception & msg){
		cout << msg.what() << endl;
	}
	return 1;
}

int Rename(FileSystem & FS){
	vector<const string> data;
	dataRename(data);
	try{
		FS.Run("rename", data);
		cout << "File was renamed!!!" << endl;
	}
	catch (exception & msg){
		cout << msg.what() << endl;
	}
	return 1;
}

int ShowFileSystem(FileSystem & FS){
	string buf;
	vector<const string> data;
	try{
		FS.Run("show", data);
	}
	catch (exception & msg){
		cout << msg.what() << endl;
	}
	return 1;
}

int Delete(FileSystem & FS){
	vector<const string> data;
	dataDelete(data);
	try{
		FS.Run("delete", data);
		cout << "File was deleted!!!" << endl;
	}
	catch (exception & msg){
		cout << msg.what() << endl;
	}
	return 1;
}

int Move(FileSystem & FS){
	vector<const string> data;
	dataMove(data);
	try{
		FS.Run("move", data);
		cout << "Catalog was replaced!!!" << endl;
	}
	catch (exception & msg){
		cout << msg.what() << endl;
	}
	return 1;
}

int Copy(FileSystem & FS){
	vector<const string> data;
	dataCopy(data);
	try{
		FS.Run("copy", data);
		cout << "Catalog was copied!!!" << endl;
	}
	catch (exception & msg){
		cout << msg.what() << endl;
	}
	return 1;
}


int Write(FileSystem & FS){
	vector<const string> data;
	dataWrite(data);
	try{
		FS.Run("write", data);
		cout << "Data were written!!!" << endl;
	}
	catch (exception & msg){
		cout << msg.what() << endl;
	}
	return 1;
}

int Read(FileSystem & FS){
	string buf;
	vector<const string> data;
	dataRead(data);
	try{
		buf = FS.Run("read", data);
		cout << "Data: " << buf << endl;
	}
	catch (exception & msg){
		cout << msg.what() << endl;
	}
	return 1;
}

int ChangeUserRight(FileSystem & FS){
	vector<const string> data;
	dataChangeUserRight(data);
	try{
		FS.Run("change user right", data);
		cout << "Right was changed!!!" << endl;
	}
	catch (exception & msg){
		cout << msg.what() << endl;
	}
	return 1;
}

int ChangeGroupRight(FileSystem & FS){
	vector<const string> data;
	dataChangeGroupRight(data);
	try{
		FS.Run("change group right", data);
		cout << "Right was changed!!!" << endl;
	}
	catch (exception & msg){
		cout << msg.what() << endl;
	}
	return 1;
}


int AddUserInGroup(FileSystem & FS){
	vector<const string> data;
	dataAddUserInGroup(data);
	try{
		FS.Run("add user in group", data);
		cout << "User was added in group!!!" << endl;
	}
	catch (exception & msg){
		cout << msg.what() << endl;
	}
	return 1;
}

int DeleteUserFromGroup(FileSystem & FS){
	vector<const string> data;
	dataDeleteUserFromGroup(data);
	try{
		FS.Run("delete user from group", data);
		cout << "User was deleted from group!!!" << endl;
	}
	catch (exception & msg){
		cout << msg.what() << endl;
	}
	return 1;
}

int DeleteUser(FileSystem & FS){
	string buf;
	int i, j;
	vector<const string> data;
	cout << "Enter name of the user you want to delete: ";
	getline(cin, buf, cin.widen('\n'));
	data.push_back(buf);
	try{
		FS.Run("delete user", data);
		string name;
		vector<const string>::iterator ptr;
		for (ptr = users.begin() + 1; ptr != users.end(); ++ptr){
			name = (*ptr);
			for (j = 0; j < name.size(); j++)
				if (name[j] == ' ')
					break;
			name.erase(0, j + 1);
			if (buf == name){
				users.erase(ptr);
				break;
			}
		}
		cout << "User was deleted!!!" << endl;
	}
	catch (exception & msg){
		cout << msg.what() << endl;
	}
	return 1;
}

int DeleteGroup(FileSystem & FS){
	vector<const string> data;
	dataDeleteGroup(data);
	try{
		FS.Run("delete group", data);
		cout << "Group was deleted!!!" << endl;
	}
	catch (exception & msg){
		cout << msg.what() << endl;
	}
	return 1;
}

int CreateCommand(FileSystem & FS){
	map<const string, int(*)(vector<const string> &)> f;
	f.insert(make_pair("create file", &dataCreateFile));
	f.insert(make_pair("create catalog", &dataCreateCatalog));
	f.insert(make_pair("rename", &dataRename));
	f.insert(make_pair("delete", &dataDelete));
	f.insert(make_pair("move", &dataMove));
	f.insert(make_pair("copy", &dataCopy));
	f.insert(make_pair("show", &dataShowFileSystem));
	f.insert(make_pair("write", &dataWrite));
	f.insert(make_pair("read", &dataRead));
	vector<const string> data;
	string buf, name;
	cout << "Enter name of new command: ";
	getline(cin, name, cin.widen('\n'));
	data.push_back(name);
	bool fl = false;
	for (int i = 0; i < new_commands.size(); i++)
		if (name == new_commands[i]){
			fl = true;
			break;
		}
	if (f.find(name) != f.end() || fl){
		cout << "Command with this name already exists!!!" << endl;
		return 1;
	}
	cout << "Enter 'end' to finish entering!!!" << endl;
	do{
		cout << "Enter name of existing command: ";
		getline(cin, buf, cin.widen('\n'));
		if (buf == "end")
			break;
		if (!FS.get_table_command().Find(buf)){
			cout << "Command was not found!!!" << endl;
			return 1;
		}
		data.push_back(buf);
		if (f.find(buf) == f.end())
			dataCompositeCommand(FS, buf, data);
		else
			if (!f.find(buf)->second(data))
				return 0;
	} while (1);
	new_commands.push_back(name);
	try{
		FS.Run("create command", data);
		cout << "Command was created!!!" << endl;
	}
	catch (exception & msg){
		cout << msg.what() << endl;
	}
	return 1;
}

int RunCompositeCommand(FileSystem & FS){
	vector<const string> data;
	string buf, name;
	int number, i;
	bool fl = false;
	cout << "Enter name of composite command: ";
	getline(cin, name, cin.widen('\n'));
	if (!FS.get_table_command().Find(name) || typeid(*(FS.get_table_command().Find(name))) != typeid(CompositeCommand)){
		cout << "Command was not found!!!" << endl;
		return 1;
	}
	try{
		if (FS.Find_command(name))
			number = FS.Find_command(name)->get_number_of_operands();
		for (i = 1; i <= number; i++){
			cout << "Enter meaning of $" << i << ": ";
			buf.clear();
			getline(cin, buf, cin.widen('\n'));
			data.push_back(buf);
		}
		FS.Run(name, data);
		cout << "Command finished successfully!!!" << endl;
	}
	catch (exception & msg){
		cout << msg.what() << endl;
	}
	return 1;
}

int DeleteCommand(FileSystem & FS){
	vector<const string> data;
	string name;
	cout << "Enter name of composite command: ";
	getline(cin, name, cin.widen('\n'));
	data.push_back(name);
	bool fl = false;
	if (!FS.get_table_command().Find(name) || typeid(*(FS.get_table_command().Find(name))) != typeid(CompositeCommand)){
		cout << "Command was not found!!!" << endl;
		return 1;
	}
	try{
		FS.Run("delete command", data);
		cout << "Command was deleted!!!" << endl;
	}
	catch (exception & msg){
		cout << msg.what() << endl;
	}
	return 1;
}

int LogOn(FileSystem & FS){
	int(*menu1[])(FileSystem &) = { NULL, CreateFile, CreateCatalog, Delete, Rename, ShowFileSystem, Move, Copy, Write,
		Read, ChangeUserRight, ChangeGroupRight, DeleteUser, DeleteGroup, AddUserInGroup, DeleteUserFromGroup, 
		CreateCommand, RunCompositeCommand, DeleteCommand };
	int rc, i;
	rc = dialog(users);
	try{
		string s = users[rc];
		for (i = 0; i < s.size() && s[i] != ' '; i++);
		s.erase(s.begin(), s.begin() + i + 1);
		FS.Start_work_with_user(s);
	}
	catch (exception & msg){
		cout << msg.what() << endl;
		return 1;
	}
	while (rc = dialog(menu2))
		if (!menu1[rc](FS))
			break;
	FS.Finish_work_with_user();
	return 1;
}

int NewUser(FileSystem & FS){
	int(*menu1[])(FileSystem &) = { NULL, CreateFile, CreateCatalog, Delete, Rename, ShowFileSystem, Move, Copy, Write,
		Read, ChangeUserRight, ChangeGroupRight, DeleteUser, DeleteGroup, AddUserInGroup, DeleteUserFromGroup,
		CreateCommand, RunCompositeCommand, DeleteCommand };
	int rc;
	string buf;
	cout << "Enter name of new user: ";
	getline(cin, buf, cin.widen('\n'));
	try{
		FS.Add_new_user(buf);
		stringstream ss;
		ss << users.size() << ". " << buf;
		users.push_back(ss.str());
		FS.Start_work_with_user(buf);
		cout << "New user was created!!!" << endl;
	}
	catch (exception & msg){
		cout << msg.what() << endl;
		return 1;
	}
	while (rc = dialog(menu2))
		if (!menu1[rc](FS))
			break;
	FS.Finish_work_with_user();
	return 1;
}

int NewGroup(FileSystem & FS){
	string buf;
	cout << "Enter name of new group: ";
	getline(cin, buf, cin.widen('\n'));
	try{
		FS.Add_new_group(buf);
		stringstream ss;
		ss << groups.size() + 1 << ". " << buf;
		groups.push_back(ss.str());
		cout << "New group was created!!!" << endl;
	}
	catch (exception & msg){
		cout << msg.what() << endl;
	}
	return 1;
}

int _tmain(int argc, _TCHAR* argv[]){
	FileSystem FS;
	int rc;
	int(*main_menu[])(FileSystem &) = { NULL, LogOn, NewUser, NewGroup };
	FS.ReadFromFile("FileForFS.dat");
	while (rc = dialog(menu))
		if (!main_menu[rc](FS))
			break;
	FS.SaveInFile("FileForFS.dat");
	return 0;
}