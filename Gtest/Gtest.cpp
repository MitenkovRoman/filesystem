
#include "stdafx.h"
#include "..\FileSystem\FileSystem.h"
#include "gtest\gtest.h"

TEST(File, Constructors){
	AccessFileRights rights1(1, true), rights2(1, false);
	vector<AccessFileRights> v1, v2;
	v1.push_back(rights1);
	v2.push_back(rights2);
	UsualFile u_file(1, 2, v1, v2);
	EXPECT_EQ(1, u_file.get_ID_user());
	EXPECT_EQ(2, u_file.get_ID_group());
	EXPECT_EQ(true, u_file.get_user_rights(1).rights.find("change")->second);
	EXPECT_EQ(true, u_file.get_user_rights(1).rights.find("change meta data")->second);
	EXPECT_EQ(true, u_file.get_user_rights(1).rights.find("write")->second);
	EXPECT_EQ(true, u_file.get_user_rights(1).rights.find("read")->second);
	EXPECT_EQ(true, u_file.get_user_rights(1).rights.find("change rights")->second);
	AccessCatalogRights r1(0, true), r2(1, false);
	vector<AccessCatalogRights> vec1, vec2;
	vec1.push_back(r1);
	vec2.push_back(r2);
	Catalog ctlg(0, 1, vec1, vec2);
	EXPECT_EQ(0, ctlg.get_ID_user());
	EXPECT_EQ(1, ctlg.get_ID_group());
	EXPECT_EQ(true, ctlg.get_user_rights(0).rights.find("pass")->second);
	EXPECT_EQ(true, ctlg.get_user_rights(0).rights.find("change")->second);
	EXPECT_EQ(true, ctlg.get_user_rights(0).rights.find("change rights")->second);
	EXPECT_EQ(true, ctlg.get_user_rights(0).rights.find("read")->second);
	EXPECT_EQ(true, ctlg.get_user_rights(0).rights.find("write")->second);
	EXPECT_EQ(true, ctlg.get_user_rights(0).rights.find("change meta data")->second);
}

TEST(Owner, Constructors){
	User user("I");
	user.Add(0);
	EXPECT_EQ(0, user.get_ID());
	EXPECT_EQ("I", user.get_name());
	EXPECT_EQ(0, user.get_groups()[0]);
	user.Delete(0);
	EXPECT_EQ(0, user.get_groups().size());
	Group group("I");
	group.Add(0);
	EXPECT_EQ(0, group.get_ID());
	EXPECT_EQ("I", group.get_name());
	EXPECT_EQ(0, group.get_users()[0]);
	group.Delete(0);
	EXPECT_EQ(0, group.get_users().size());

}

TEST(UsualFile, ReadWrite){
	AccessFileRights rights1(1, true), rights2(1, false);
	vector<AccessFileRights> v1, v2;
	v1.push_back(rights1);
	v2.push_back(rights2);
	UsualFile u_file(1, 2, v1, v2);
	u_file.Write("aaabbbcccddd", 1);
	EXPECT_EQ("aaabbb", u_file.Read(1, 6));
	EXPECT_EQ("aaabbbcccddd", u_file.Read(1, 20));
	EXPECT_EQ("cccddd", u_file.Read(7, 20));
	u_file.Write("eee", 4);
	EXPECT_EQ("aaaeeecccddd", u_file.Read(1, 20));
	u_file.Write("fff", 100);
	EXPECT_EQ("aaaeeecccdddfff", u_file.Read(1, 20));
}

TEST(File, ChangeCheckRights){
	AccessFileRights rights1(1, true), rights2(1, false);
	vector<AccessFileRights> v1, v2;
	v1.push_back(rights1);
	v2.push_back(rights2);
	UsualFile u_file(1, 2, v1, v2);
	EXPECT_EQ(true, u_file.get_user_rights(1).rights.find("change")->second);
	u_file.change_user_right(1, "change");
	EXPECT_EQ(false, u_file.get_user_rights(1).rights.find("change")->second);
	vector<int> n;
	EXPECT_EQ(false, u_file.check_right(1,"change", n));
}

TEST(Catalog, AddDelete){
	AccessFileRights rights1(1, true), rights2(1, false);
	vector<AccessFileRights> v1, v2;
	v1.push_back(rights1);
	v2.push_back(rights2);
	UsualFile u_file(1, 2, v1, v2);
	AccessCatalogRights r1(0, true), r2(1, false);
	vector<AccessCatalogRights> vec1, vec2;
	vec1.push_back(r1);
	vec2.push_back(r2);
	Catalog ctlg(0, 1, vec1, vec2);
	ctlg.add_to_catalog("1", &u_file);
	EXPECT_EQ(1, ctlg.get_catalog().find("1")->second->get_ID_user());
	EXPECT_EQ(2, ctlg.get_catalog().find("1")->second->get_ID_group());
	EXPECT_EQ(1, ctlg.get_size());
	ctlg.delete_from_catalog("1");
	EXPECT_EQ(0, ctlg.get_size());
}

TEST(TableUser, AddDelete){
	TableUser table;
	EXPECT_EQ(0, table.number());
	User user1("1"), user2("2");
	table.Add(&user1);
	EXPECT_EQ(1, table.number());
	EXPECT_EQ("1", table.Find(user1.get_ID())->get_name());
	table.Add(&user2);
	EXPECT_EQ(2, table.number());
	table.Delete("1");
	cout << "\t\t\t" << table.Find("2")->get_ID() << endl;
	EXPECT_EQ(1, table.number());
	EXPECT_EQ(nullptr, table.Find(user1.get_ID()));
}

TEST(TableGroup, AddDelete){
	TableGroup table;
	EXPECT_EQ(0, table.number());
	Group group1("1"), group2("2");
	table.Add(&group1);
	EXPECT_EQ(1, table.number());
	EXPECT_EQ("1", table.Find(group1.get_ID())->get_name());
	table.Add(&group2);
	EXPECT_EQ(2, table.number());
	table.Delete("1");
	cout << "\t\t\t" << table.Find("2")->get_ID() << endl;
	EXPECT_EQ(1, table.number());
	EXPECT_EQ(nullptr, table.Find(group1.get_ID()));
}

TEST(Vector, AllTests){
	Vector<int*> vec, vec1;
	int n1 = 1, n2 = 2, n3 = 3, n4 = 4, n5 = 5;
	EXPECT_EQ(0, vec.size());
	vec.push_back(&n1);
	vec.push_back(&n2);
	vec.push_back(&n3);
	vec.push_back(&n4);
	vec.push_back(&n5);
	vec1.push_back(&n3);
	vec1.push_back(&n4);
	vec1.push_back(&n5);
	VectorIt<int*> ptr = vec.begin();
	EXPECT_EQ(1, *(*ptr));
	ptr++;
	EXPECT_EQ(2, *(*ptr));
	EXPECT_EQ(5, vec.size());
	++ptr;
	EXPECT_EQ(3, *(*ptr));
	vec.erase(ptr);
	EXPECT_EQ(4, vec.size());
	EXPECT_EQ(4, *(*ptr));
	vec.erase(0, 3);
	EXPECT_EQ(1, vec.size());
	ptr = vec.begin();
	EXPECT_EQ(5, *(*ptr));
	vec = vec1;
	EXPECT_EQ(vec.size(), vec1.size());
	Vector<int *>::Iterator ptr1 = vec.begin(), ptr2 = vec1.begin();
	EXPECT_EQ(*(*ptr1), *(*ptr2));
	ptr1++;
	ptr2++;
	EXPECT_EQ(*(*ptr1), *(*ptr2));
	vec = vec;
	EXPECT_EQ(3, vec.size());
	EXPECT_EQ(3, *(*vec.begin()));
}

TEST(FileSystem, CreateFileCatalog){
	FileSystem FS;
	vector<const string> f_data, c_data;
	f_data.push_back("file1");
	f_data.push_back("root");
	f_data.push_back("");
	FS.Start_work_with_user("Administrator");
	FS.CreateFile(f_data);
	EXPECT_EQ("file1", FS.get_root()->get_catalog().find(f_data[0])->first);
	c_data.push_back("ctl1");
	c_data.push_back("root");
	c_data.push_back("");
	FS.CreateCatalog(c_data);
	EXPECT_EQ("ctl1", FS.get_root()->get_catalog().find(c_data[0])->first);
}

TEST(FileSystem, CopyFileCatalog){
	FileSystem FS;
	vector<const string> f_data, c_data, copy;
	f_data.push_back("file1");
	f_data.push_back("root");
	f_data.push_back("");
	FS.Start_work_with_user("Administrator");
	FS.CreateFile(f_data);
	EXPECT_EQ("file1", FS.get_root()->get_catalog().find(f_data[0])->first);
	c_data.push_back("ctl1");
	c_data.push_back("root");
	c_data.push_back("");
	FS.CreateCatalog(c_data);
	EXPECT_EQ("ctl1", FS.get_root()->get_catalog().find(c_data[0])->first);
	copy.push_back("root\\file1");
	copy.push_back("root\\ctl1");
	FS.Copy(copy);
	Catalog * ctlg = static_cast<Catalog*>(FS.get_root()->get_catalog().find("ctl1")->second);
	EXPECT_EQ("file1", ctlg->get_catalog().find("file1")->first);
	EXPECT_EQ("file1", FS.get_root()->get_catalog().find("file1")->first);
}

TEST(FileSystem, MoveFileCatalog){
	FileSystem FS;
	vector<const string> f_data, c_data, move;
	f_data.push_back("file1");
	f_data.push_back("root");
	f_data.push_back("");
	FS.Start_work_with_user("Administrator");
	FS.CreateFile(f_data);
	EXPECT_EQ("file1", FS.get_root()->get_catalog().find(f_data[0])->first);
	c_data.push_back("ctl1");
	c_data.push_back("root");
	c_data.push_back("");
	FS.CreateCatalog(c_data);
	EXPECT_EQ("ctl1", FS.get_root()->get_catalog().find(c_data[0])->first);
	move.push_back("root\\file1");
	move.push_back("root\\ctl1");
	FS.Move(move);
	Catalog * ctlg = static_cast<Catalog*>(FS.get_root()->get_catalog().find("ctl1")->second);
	EXPECT_EQ("file1", ctlg->get_catalog().find("file1")->first);
	map<const string, File *>::const_iterator ptr = FS.get_root()->get_catalog().end();
	EXPECT_EQ(ptr, FS.get_root()->get_catalog().find("file1"));
}

TEST(FileSystem, DeleteFileCatalog){
	FileSystem FS;
	vector<const string> f_data, c_data, del;
	f_data.push_back("file1");
	f_data.push_back("root");
	f_data.push_back("");
	FS.Start_work_with_user("Administrator");
	FS.CreateFile(f_data);
	EXPECT_EQ("file1", FS.get_root()->get_catalog().find(f_data[0])->first);
	c_data.push_back("ctl1");
	c_data.push_back("root");
	c_data.push_back("");
	FS.CreateCatalog(c_data);
	del.push_back("root\\file1");
	FS.Delete(del);
	map<const string, File *>::const_iterator ptr = FS.get_root()->get_catalog().end();
	EXPECT_EQ(ptr, FS.get_root()->get_catalog().find("file1"));
	del[0] = "root\\ctl1";
	FS.Delete(del);
	ptr = FS.get_root()->get_catalog().end();
	EXPECT_EQ(ptr, FS.get_root()->get_catalog().find("ctl1"));
}

int _tmain(int argc, _TCHAR* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

